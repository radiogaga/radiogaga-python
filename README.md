# RadioGAGA

Python package to process TBB datas.

Différents script existant se trouvents dans *scripts/*  
Différentes fonctions et classes se trouvent dans *radiogaga/*

## Install requirements
```bash
pip install -r requirements.txt
```

## On a nancep
To use it on a nancep it's easier to work in a virtualenv : 
```bash
python3.8 -m venv venv
source venv/bin/activate
python -m pip install --upgrade pip
pip install -r requirements.txt
```


## TODO

- Ajouter titres,echelles etc. sur le graphe du bas + ligne vertical à l'échantillon **707**
- Ajouter points cardinaux sur le farfield
- Polar X,Y. Changement seulement quand la modif firmware sera faite ? 
- Base de données ajuster les champs avant mise en prod
- Creer une fonction qui permet de retrouver le path d'un événement juste à partir de son nom et de l'obs.  Afin d'éviter de mettre le path complet dans la base pour chaque événement, trop redondant.
- Finir script permettant de postprocesser tous les événement passés (scripts/services/past_obs.py)
- Modifier le pipeline ou faire un script à part, permettant de processé une liste d'événement exporté depuis graphana(selected.csv) mais sur une multitude d'observation. Actuellement on peut uniquement faire ça dans une obs donnée.



