
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import glob

import argparse
import os
from tqdm import tqdm
                    

import context

from radiogaga import TBBurst_lib as tbb
from radiogaga import pixel  
from radiogaga import near_field as nf
from radiogaga import rfi_multi as rfi
from radiogaga import pointing




if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Plot all MR of an eparse trig')
  parser.add_argument('path', help='Path')
  args = parser.parse_args()


  board_cfg = tbb.load_yaml()
  MR_used = []
  for board in board_cfg.values():
    MRs = [b for b in board['MR'] if b != -1]
    MR_used += MRs
  MR_used.sort()
  MR_used.remove(101)
  MR_used.remove(102)
  npzs = glob.glob(f"{args.path}/output/bin/*.npz")
  heat_map_shape = np.load(npzs[0]).shape
  heat_map_average = np.zeros(heat_map_shape)
  npzs.sort()
  nearfield = nf.nearfield(nf.Mode.NEAR,MR_used)
  for npz in tqdm(npzs):
    heat_map = np.load(npz)
    max = heat_map.max()
    min = heat_map.min()
    heat_map = (heat_map - min) / (max - min)    
    heat_map_average += heat_map 

    #fig = plt.figure()
    #ax = fig.add_subplot()
    #nearfield.plot(heat_map_average,max,min,ax)
    #plt.show()

  max = heat_map_average.max()
  min = heat_map_average.min()

  print(heat_map_average.max()),print(heat_map_average.min())
  fig = plt.figure()
  ax = fig.add_subplot()
  nearfield.plot(heat_map_average,max,min,ax)
  plt.show()