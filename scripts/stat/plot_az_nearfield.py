
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import glob

import argparse
import os
from tqdm import tqdm
                    

import context

from radiogaga import TBBurst_lib as tbb
from radiogaga import pixel  
from radiogaga import near_field as nf
from radiogaga import rfi_multi as rfi
from radiogaga import pointing




if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Plot all MR of an eparse trig')
  parser.add_argument('path', help='Path')
  args = parser.parse_args()


  board_cfg = tbb.load_yaml()
  MR_used = []
  for board in board_cfg.values():
    MRs = [b for b in board['MR'] if b != -1]
    MR_used += MRs
  MR_used.sort()
  MR_used.remove(101)
  MR_used.remove(102)

  azs = np.loadtxt(f"{args.path}/output/list_az_near.txt",comments="#",usecols=(2))
  print(azs,azs.shape)
  azs_rad = azs * np.pi / 180
  fig = plt.figure()
  ax = fig.add_subplot(projection='polar')
  ax.set_theta_zero_location("N")
  ax.set_theta_direction(-1)
  #ax.plot(azs*np.pi/180 ,np.ones(azs.shape),'.')
  alpha= 1 / azs.shape[0]
  print(alpha)
  ax.vlines(azs_rad,0,1,color='red',alpha=0.05)
  plt.show()
  


