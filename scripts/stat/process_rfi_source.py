import glob 
import argparse
from tqdm import tqdm
import numpy as np  
import pandas as pd
import os
from glob import glob
import context
from radiogaga import TBBurst_lib as tbb

parser = argparse.ArgumentParser(description='Convert tbb_dump.bin in Spectrogram')
parser.add_argument('path', help='Path')
parser.add_argument('event_path', help='Path')

args = parser.parse_args()

lists = glob(f'{args.path}/*.csv')
nanoseconds = []
for list in lists:
  events = pd.read_csv(list)
  for events_idx,event in events.iterrows():
    zip = glob(f'{args.event_path}/**/{event["name"]}*.zip')
    files,dat = tbb.load_eparse_zip(zip[0])
    print(dat['tbb_nano'])
    nanoseconds.append(dat['tbb_nano'])
    np.savetxt(f'{args.path}/nanoseconds.txt',nanoseconds,fmt='%d')
  
sigma = np.std(np.array(nanoseconds)) #/ 256
var = np.var(np.array(nanoseconds))   #/ 256 

print(sigma,var)
print(1/sigma,1/var)
