import glob 
import argparse
from multiprocessing.resource_sharer import stop
import matplotlib as mpl
from tqdm import tqdm
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from datetime import datetime


import shutil
import os

import context
from radiogaga import TBBurst_lib as tbb
from  radiogaga.database import Database


parser = argparse.ArgumentParser(description='Convert tbb_dump.bin in Spectrogram')
parser.add_argument('--path', help='Path or source zips')
parser.add_argument('--export', help='Path of result destination')
parser.add_argument('startTime',help="Date of First events")
parser.add_argument('--stopTime',help="Date of Last events",default=datetime.now())

args = parser.parse_args()

if args.export is not None:
  if os.path.isdir(f'{args.export}/pixel_selected'):
    print(f'Dir pixel_selected already exists')
  else:
    os.mkdir(f'{args.export}/pixel_selected')

  if os.path.isdir(f'{args.export}/zips'):
    print(f'Dir zips already exists')
  else:
    os.mkdir(f'{args.export}/zips')

  zone = args.export.split('/')[-2]

import env
db = Database(env.HOST_DB,8086,env.USER_DB,env.PSWD_DB,env.DB)
fields = '"name","pos_x","pos_y","upd_count"'
startime = datetime.strptime(args.startTime,"%Y-%m-%dT%H:%M:%SZ")
if type(args.stopTime) != type(startime):
  stoptime = datetime.strptime(args.stopTime,"%Y-%m-%dT%H:%M:%SZ")
else:
  stoptime = args.stopTime
df = db.query_between_start_end("TBB_events",startime,stop=stoptime,fields=fields)
df['pos_x'] = df['pos_x'].astype(int)
df['pos_y'] = df['pos_y'].astype(int)

print(df)

mesh_size = (386,348)

def mouse_event(event):
  x = int(event.xdata)
  y = int(event.ydata)
  result = df.query(f'pos_x == {x} and pos_y == {y}')['name']
  print(result)
  fname = f'{x}_{y}.csv'
  if os.path.isfile(f'{args.export}/pixel_selected/{fname}'):
    print(f'{args.export}/pixel_selected/{fname} already exported')
  else:  
    result.to_csv(f'{args.export}/pixel_selected/{fname}',index=False)
    print(f'{args.export}/pixel_selected/{fname} sucessfully exported')
        
if True:    
  fig = plt.figure(figsize=(15, 10))
  ax = fig.add_subplot(111)
  if args.export is not None:
    cid = fig.canvas.mpl_connect('button_press_event', mouse_event)


  USN_img = plt.imread(f'../aerial.tif')
  #background_view = pathlib.Path('../aerial.tif')
  #with rasterio.open(background_view.as_posix()) as r:
  #    USN_ext = tuple(r.bounds)
  #    USN_crs = r.crs
  #    USN_img = r.read((1,2,3)).transpose((1,2,0))

  
  ax.imshow(USN_img, alpha=1,extent=[0,mesh_size[0],0,mesh_size[1]])
  
  # MAtlolib 2d
  #hist = plt.hist2d(df['pos_x'], df['pos_y'], bins=(mesh_size[0],mesh_size[1]),cmin=5)  
  hist = ax.hist2d(df['pos_x'], df['pos_y'], bins=(mesh_size[0],mesh_size[1]),norm=mpl.colors.LogNorm(),cmin=1,cmap=plt.get_cmap('hot'))   

  plt.colorbar(hist[3],label='Events counts',)
  #plt.tight_layout()
  if args.export is not None:
    fig.suptitle(f'Zone : {zone}', fontsize=20)
    ax.set_title('Cliquer sur les pixels d\' une même zone pour exporter les événements associés', fontsize=16)
  #ax.set_title('Nearfield of TBB trig during april 2022', fontsize=16)
  #plt.savefig(f'{args.export}/nearfield_global_2022_04.png',dpi=300)
  plt.show()
  
if args.export is not None:
  lists = glob.glob(f'{args.export}/pixel_selected/*.csv')
  output_df = pd.DataFrame(columns=['upd_count','name'])

  for list in lists:
    events = pd.read_csv(list)
    print(events)
    # Add the date of the events to sort and class it
    try:
      events['date'] = events['name'].apply(lambda x: x[0:8])
    except:
      print("One or more name are not rechonysed")
    output_df = pd.concat((output_df,events),ignore_index=True)

  # Sort by name 
  output_df = output_df.sort_values(by=['name'])
  # Save to csv
  output_df.to_csv(f'{args.export}/events_{zone}.txt',index=False)

  # Create rapport
  print('Generating rapport')
  with open(f'{args.export}/Rapport_{zone}.md', 'w+') as f:
    visible_days = f'{output_df["date"].value_counts()}'
    visible_days = visible_days.replace('\n','\n\n')
    f.write(f'# Zone {zone}\n')
    # Nb of events
    f.write(f'## Number of events\n')
    f.write(f'{output_df.shape[0]}\n')
    # Nb of days
    f.write(f'## Visible days :  \n')
    f.write(f'{visible_days}\n') 
    # Nb of days with events

  #Copy events in zips dir
  print('Copying events in zips dir')
  for zip in tqdm(output_df['name'].unique()):
    print(f'{args.path}**/{zip}.zip')
    try: 
      path = glob.glob(f'{args.path}/**/{zip}.zip')[0]
      if os.path.isfile(f'{path}'):
        shutil.copy2(path, f'{args.export}/zips/{zip}.zip')
    except:
      print(f'Zip {zip} not found')



