import glob 
import argparse
from tqdm import tqdm
import os

parser = argparse.ArgumentParser(description='Convert tbb_dump.bin in Spectrogram')
parser.add_argument('path', help='Path')

args = parser.parse_args()

zones = glob.glob(f'{args.path}/source*')
print(zones)
input("Press Enter to continue...")

for zone in zones:

  print(f'processing day {os.path.basename(zone)}')
  os.system(f'python ../eparse/resume_rfi.py near {zone}/zips/ 20 1 --nofevents 100')
