import glob 
import argparse
import matplotlib as mpl
from tqdm import tqdm
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import rasterio
import pathlib
import seaborn as sns
import shutil
import os

import context
from radiogaga import TBBurst_lib as tbb
from radiogaga.near_field import nearfield


parser = argparse.ArgumentParser(description='Convert tbb_dump.bin in Spectrogram')
parser.add_argument('path', help='Path')
parser.add_argument('output_path', help='Path')

args = parser.parse_args()

if os.path.isdir(f'{args.output_path}/pixel_selected'):
  print(f'Dir pixel_selected already exists')
else:
  os.mkdir(f'{args.output_path}/pixel_selected')

if os.path.isdir(f'{args.output_path}/zips'):
  print(f'Dir zips already exists')
else:
  os.mkdir(f'{args.output_path}/zips')

if os.path.exists(f'{args.path}/output/list_pos_near.txt'):
  print('Only 1 dir to process')
  df = pd.read_csv(f'{args.path}/output/list_pos_near.txt',names=('name','x','y'),sep=' ',comment='#')
  zone = args.output_path.split('/')[-2]
  singleDir = True
else:
  days = glob.glob(f'{args.path}/*')
  print(*days,sep='\n')
  zone = args.output_path.split('/')[-2]
  print(zone)

  for idx,day in enumerate(days):
    # check if file exist
    if os.path.isfile(f'{day}/output/list_az_near.txt'):
      if idx == 0:
        df = pd.read_csv(f'{day}/output/list_pos_near.txt',names=('name','x','y'),sep=' ',comment='#')
      else:
        df = pd.concat((df,pd.read_csv(f'{day}/output/list_az_near.txt',names=('name','x','y'),sep=' ',comment='#')),ignore_index=True)
    else:
      print(f'{day}/output/list_az_near.txt not found')



mesh_size = (386,348)

def mouse_event(event):
  x = int(event.xdata)
  y = int(event.ydata)
  result = df.query(f'x == {x} and y == {y}')['name']
  fname = f'{x}_{y}.csv'
  if os.path.isfile(f'{args.output_path}/pixel_selected/{fname}'):
    print(f'{args.output_path}/pixel_selected/{fname} already exported')
  else:  
    result.to_csv(f'{args.output_path}/pixel_selected/{fname}',index=False)
    print(f'{args.output_path}/pixel_selected/{fname} sucessfully exported')
        
if True:    
  fig = plt.figure(figsize=(15, 10))
  ax = fig.add_subplot(111)
  cid = fig.canvas.mpl_connect('button_press_event', mouse_event)


  USN_img = plt.imread(f'../aerial.tif')
  #print(USN_img.shape)
  #background_view = pathlib.Path('../aerial.tif')
  #with rasterio.open(background_view.as_posix()) as r:
  #    USN_ext = tuple(r.bounds)
  #    USN_crs = r.crs
  #    USN_img = r.read((1,2,3)).transpose((1,2,0))

  
  # MAtlolib 2d
  #hist = plt.hist2d(df['x'], df['y'], bins=(mesh_size[0],mesh_size[1]),cmin=5)  
  hist = ax.hist2d(df['x'], df['y'], bins=(mesh_size[0],mesh_size[1]),norm=mpl.colors.LogNorm(),cmin=1,cmap=plt.get_cmap('hot'),range=([0,mesh_size[0]],[0,mesh_size[1]]))   
  plt.colorbar(hist[3],label='Events counts',)
  ax.imshow(USN_img, alpha=1,extent=[0,mesh_size[0],0,mesh_size[1]],aspect='equal')

  
  #plt.tight_layout()
  fig.suptitle(f'Zone : {zone}', fontsize=20)
  ax.set_title('Cliquer sur les pixels d\' une même zone pour exporter les événements associés', fontsize=16)
  #ax.set_title('Nearfield of TBB trig during april 2022', fontsize=16)
  #plt.savefig(f'{args.output_path}/nearfield_global_2022_04.png',dpi=300)
  plt.show()
  

lists = glob.glob(f'{args.output_path}/pixel_selected/*.csv')
output_df = pd.DataFrame(columns=['date','name'])

for list in lists:
  events = pd.read_csv(list)
  # Add the date of the events to sort and class it
  events['date'] = events['name'].apply(lambda x: x[0:8])
  output_df = pd.concat((output_df,events),ignore_index=True)

# Sort by name 
output_df = output_df.sort_values(by=['name'])
# Save to csv
output_df.to_csv(f'{args.output_path}/events_{zone}.txt',index=False)

# Create rapport
print('Generating rapport')
with open(f'{args.output_path}/Rapport_{zone}.md', 'w+') as f:
  visible_days = f'{output_df["date"].value_counts()}'
  visible_days = visible_days.replace('\n','\n\n')
  f.write(f'# Zone {zone}\n')
  # Nb of events
  f.write(f'## Number of events\n')
  f.write(f'{output_df.shape[0]}\n')
  # Nb of days
  f.write(f'## Visible days :  \n')
  f.write(f'{visible_days}\n') 
  # Nb of days with events

# Copy events in zips dir
print('Copying events in zips dir')
for zip in tqdm(output_df['name'].unique()):
  if singleDir:
    path = glob.glob(f'{args.path}/{zip}.zip')[0]
  else:
    h = glob.glob(f'{args.path}/**/{zip}.zip')[0]
    
  if os.path.isfile(f'{path}'):
    shutil.copy2(path, f'{args.output_path}/zips/{zip}.zip')  



