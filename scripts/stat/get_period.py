from matplotlib.pyplot import axis
import numpy as np
import math
from functools import reduce
import argparse
from tqdm import tqdm
from matplotlib import pyplot as plt


parser = argparse.ArgumentParser(description='Convert tbb_dump.bin in Spectrogram')
parser.add_argument('path', help='Path')

args = parser.parse_args()

ma_list = (4,2,2,8,6,4,6,8,2,2,4,4,10,6,4,2,6,8,2,10,2,4,6,8,4,2,4,4,2,6,8,8,6,6,10,2,2,2,2,2,4,6,2,4,2,8,8,8,8,8)
ma_list = (3,6,9,9,9,6,3,3,0,6,9,0,6,3,0,3,6,9,0,3,6,9,9,9,9,3,6,9,3,0,6,9,3,0,6,3,9,0,3,9,6)
#ma_list = (0,50,100,150,200,150,200,250,300,350,50,0,50,50,100,150,300,400,450,500,550,)
ma_list = np.array(ma_list)
print(ma_list)
ma_list = np.loadtxt(f'{args.path}/nanoseconds.txt',dtype=int)
ma_list = ma_list[0:300]
diff = np.abs(np.diff(ma_list,n=1))
diff = np.sort(diff)
median = np.median(diff)
mean = np.mean(diff)
print(f'Median: {median}')
print(f'Mean: {mean}')
plt.plot(ma_list)
plt.plot(diff)
#plt.show()

print(f'Freq median = {1/median*1e9}')
print(f'Freq mean = {1/mean*1e9}')


diff = np.abs(np.gradient(ma_list))
diff = np.sort(diff)
median = np.median(diff)
mean = np.mean(diff)
print(f'Median: {median}')
print(f'Mean: {mean}')
plt.plot(ma_list)
plt.plot(diff)


print(f'Freq median = {1/median*1e9}')
print(f'Freq mean = {1/mean*1e9}')

plt.show()