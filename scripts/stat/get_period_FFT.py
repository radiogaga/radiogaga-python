import glob 
import argparse
from tqdm import tqdm
import numpy as np  
import pandas as pd
import os
from glob import glob
import math
import context
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

from scipy.fftpack import fft, rfft,fftfreq
from datetime import datetime
from radiogaga import TBBurst_lib as tbb

def abs2(x):
  return x.real**2 + x.imag**2

def truncate(number, decimals=0):
    """
    Returns a value truncated to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer.")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more.")
    elif decimals == 0:
        return math.trunc(number)

    factor = 10.0 ** decimals
    return math.trunc(number * factor) / factor

def safe_arange(start, stop, step):
  return step * np.arange(start / step, stop / step)

parser = argparse.ArgumentParser(description='Convert tbb_dump.bin in Spectrogram')
parser.add_argument('path', help='Path')

args = parser.parse_args()
test = np.arange(0,20,1)

zips = glob(f"{args.path}/*.zip")
zips.sort()
zips[0:1000]
#print(zips)
integration_seconde = 30
te = 0.1e-2
fe = 1/te
print(te,fe)

points =[]
points2 =[]
for z in zips:
  files, infos, parameters,processed = tbb.load_eparse_zip(z)  
  TS = infos.gps_second + truncate(infos.gps_nano/1e9,3)
  TS2 = infos.gps_second + truncate(infos.gps_nano/1e9+te,3)
  points.append(TS)
  points2.append(TS2)


first_TS = math.trunc(points[0])
last_TS = math.trunc(points[-1]) + 1

while (last_TS - first_TS) % integration_seconde != 0: 
  last_TS+=1

first_TS_human = datetime.fromtimestamp(first_TS)
first_TS_human = mdates.date2num(first_TS_human)
last_TS_human = datetime.fromtimestamp(last_TS)
last_TS_human = mdates.date2num(last_TS_human)
print(first_TS_human,last_TS_human)


#time_axis = np.arange(first_TS,last_TS,0.001) # C'est nul ca me creer des decimals WTF ! 

time_axis = np.arange(first_TS*1/te,(last_TS)*1/te,1)/(1/te)
time_axis = np.around(time_axis,3)
#for t in time_axis:
#  print(t)
  #input()


#datas = np.array((time_axis,np.zeros(time_axis.shape[0])))
datas = pd.DataFrame(columns=('time','amplitude'))
datas['time'] = time_axis
datas['amplitude'] = 0
datas = datas.set_index('time')
print(datas)


datas.loc[points] = 100
#datas.loc[points2] = -100
#plt.plot(datas['amplitude'])
#plt.show()


N = int(integration_seconde * 1/te)
datas = np.array(datas['amplitude'])
print(datas)
print(datas.shape)
fig, ax = plt.subplots()
ax.plot(time_axis,datas)
datas = np.reshape(datas,(-1,N))
#ax.plot(datas[int(0.71*datas.shape[0]),:])
print(datas.shape)
X = abs2(fft(datas,axis=1))
X = X[:,:N//2]
X = 10* np.log10(X[:,:N//2])
print(X.shape)
freq = fftfreq(N,te)[:N//2]


fig, ax = plt.subplots()
ax.imshow(X[:,:].transpose(),
              aspect='auto',
              origin='lower',
              interpolation='spline36',
              #vmax=100000,
              #vmin=10,
              extent=[first_TS_human,last_TS_human,freq[0],freq[-1]]
              )
ax.xaxis_date()

# We can use a DateFormatter to choose how this datetime string will look.
# I have chosen HH:MM:SS though you could add DD/MM/YY if you had data
# over different days.
date_format = mdates.DateFormatter('%H:%M:%S')

ax.xaxis.set_major_formatter(date_format)

# This simply sets the x-axis data to diagonal so it fits better.
fig.autofmt_xdate()

X[np.isinf(X)]=0
print(X)
X_mean = np.nanmean(X,axis=0)
print(X_mean)
print(X_mean.shape)




plt.figure(figsize = (12, 6))
plt.plot(freq, X_mean)
plt.xlabel('Freq (Hz)')
plt.ylabel('FFT Amplitude |X(freq)|')
plt.show()
