import glob 
import argparse
from tqdm import tqdm
import os

parser = argparse.ArgumentParser(description='Convert tbb_dump.bin in Spectrogram')
parser.add_argument('path', help='Path')

args = parser.parse_args()

observations = glob.glob(f'{args.path}/*')
observations.sort()
print(*observations,sep='\n')
input('Press any key to continue')

for observation in observations:
  print(f'processing observation {os.path.basename(observation)}')
  os.system(f'python resume_rfi.py near {observation} 20 0')
  #os.system(f'python ../eparse/pipeline.py new_round_pattern far {observation} 10')
