

import context
import argparse
import datetime
import time
import numpy as np


obs_start_time = "2022-09-13T15:05:00Z"
start_az = 0
stop_az = 360
resolution_az = 10 # degree
elevation = 10 # degree 
delta_pointage_min = 30# secondes
nombre_tours = 15


n_pointage = int( (stop_az - start_az) / resolution_az ) * nombre_tours
n_pointage_par_tours=n_pointage//nombre_tours
print(n_pointage_par_tours)
print(n_pointage)
duree_manip = int(n_pointage * delta_pointage_min) # secondes

start_time = datetime.datetime.strptime(obs_start_time,"%Y-%m-%dT%H:%M:%SZ")
start_time_parset = start_time.strftime("%Y%m%d_%H%M%S") 
end_time = start_time + datetime.timedelta(seconds=duree_manip)


start_time_beam0_str=(start_time                                 ).strftime("%Y-%m-%dT%H:%M:%SZ")
start_time_beam1=start_time + datetime.timedelta(seconds=60)
start_time_beam1_str=(start_time_beam1).strftime("%Y-%m-%dT%H:%M:%SZ")
print(start_time_beam0_str,start_time_beam1,end_time)



dtype={'names': ('time', 'az','el'),
            'formats': ('U64', 'f4', 'f4')}
seconde = 0
c = 0 
alzegeo = np.zeros(shape=(n_pointage),dtype=dtype)
print(alzegeo.shape)
for tour in range(nombre_tours):
    start_az=0
    for point in range(n_pointage//nombre_tours):
        
        alzegeo[c]['time'] = datetime.datetime.strftime(start_time_beam1 + datetime.timedelta(seconds=seconde),"%Y-%m-%dT%H:%M:%SZ")
        alzegeo[c]['az'] = start_az + point * resolution_az
        alzegeo[c]['el'] = elevation
        print(start_time_beam1 + datetime.timedelta(seconds=seconde))
        seconde += delta_pointage_min
        c+=1
        print(seconde)

# convert to csv  
np.savetxt(f"{start_time_parset}.csv",alzegeo,delimiter=",",fmt="%s",header = "YYYY-MM-DDTHH:NN:SSZ, az(deg), el(deg)")

parset_usr=f"""Observation.title="RFI_detection"
Observation.name="CAL_TEST_RFI_DETECTION"
Observation.contactName=tberthet
Observation.nrAnabeams=2
Observation.nrBeams=1
Observation.topic=radio_gamma

Anabeam[0].target="AZELGEO_ZENITH"
Anabeam[0].directionType=azelgeo
Anabeam[0].startTime={start_time_beam0_str}
Anabeam[0].trackingType=pointingto
Anabeam[0].duration=20s
Anabeam[0].antState=[off]


Anabeam[1].target="AZELGEO_TRACKING"
Anabeam[1].directionType=azelgeo
Anabeam[1].startTime={start_time_beam1_str}
Anabeam[1].duration={duree_manip}s
AnaBeam[1].azelFile=TBERTHET_{start_time_parset}.csv
Anabeam[1].antList=[10]
Anabeam[1].maList=[0,1,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,100,101,102,103]
Anabeam[1].attList=[39,40,42,37,42,37,39,39,37,35,36,29,29,33,28,26,24,25,23,35,30,29,27,27,22,21,24,21,22,36,29,26,23,26,20,23,22,28,27,25,23,20,21,27,33,32,27,26,27,31,31,29,28,40,35,36,29,27,27,26,23,34,30,25,36,31,29,29,25,39,38,33,34,29,28,28,22,31,31,31,31]
Anabeam[1].beamSquint=disable

Beam[0].target="AZELGEO_TRACKING"
Beam[0].subbandList=[100..400]
Beam[0].noBeam=1
Beam[0].useParentPointing=true

"""



# saver parsetusr as  .parset_usr
with open(f"{start_time_parset}.parset_user",'w') as f:
    f.write(parset_usr)











