

import context
import argparse
import datetime
import time
import numpy as np


obs_start_time = "2022-09-12T07:45:00Z"
start_az = 0
stop_az = 360
resolution_az = 10 # degree
elevation = 60 # degree 
delta_pointage_min = 20# secondes

n_pointage = int( (stop_az - start_az) / resolution_az )
print(n_pointage)
duree_manip = int(n_pointage * delta_pointage_min) # secondes

start_time = datetime.datetime.strptime(obs_start_time,"%Y-%m-%dT%H:%M:%SZ")
start_time_parset = start_time.strftime("%Y%m%d_%H%M%S")
end_time = start_time + datetime.timedelta(seconds=duree_manip)


print(start_time,end_time)


dtype={'names': ('time', 'az','el'),
            'formats': ('U64', 'f4', 'f4')}

alzegeo = np.zeros(shape=(n_pointage),dtype=dtype)

for point in range(n_pointage):
    alzegeo[point]['time'] = datetime.datetime.strftime(start_time + datetime.timedelta(seconds=point*delta_pointage_min),"%Y-%m-%dT%H:%M:%SZ")
    alzegeo[point]['az'] = start_az + point * resolution_az
    alzegeo[point]['el'] = elevation

# convert to csv  
np.savetxt(f"{start_time_parset}.csv",alzegeo,delimiter=",",fmt="%s",header = "YYYY-MM-DDTHH:NN:SSZ, az(deg), el(deg)")

parset_usr=f"""Observation.title="RFI_detection"
Observation.name="CAL_TEST_RFI_DETECTION"
Observation.contactName=tberthet
Observation.nrBeams=1
Observation.topic=radio_gamma

Anabeam[0].target="AZELGEO_TRACKING"
Anabeam[0].directionType=azelgeo
Anabeam[0].startTime={obs_start_time}
Anabeam[0].duration={duree_manip}s
AnaBeam[0].azelFile=TBERTHET_{start_time_parset}.csv
AnaBeam[0].optFrq=30

Beam[0].target="AZELGEO_TRACKING"
Beam[0].subbandList=[100..400]
Beam[0].useParentPointing=true

"""



# saver parsetusr as  .parset_usr
with open(f"{start_time_parset}.parset_user",'w') as f:
    f.write(parset_usr)











