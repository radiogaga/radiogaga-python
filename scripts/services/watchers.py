import os
from time import sleep
import logging
from glob import glob
import context
from pathlib import Path
from  radiogaga.database import Database
from radiogaga.parset import Parset
from datetime import datetime,timedelta
import subprocess

import socket
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import smtplib
#import argparse
#
#parser = argparse.ArgumentParser()
#parser.add_argument('--force', action='store_true',help="force obs, no need to be in observation time")
#parser.add_argument('--no-force', dest='force',action='store_false',help="force obs, no need to be in observation time")

import env
db = Database(env.HOST_DB,8086,env.USER_DB,env.PSWD_DB,env.NAME_DB)

def attach_file(msg, nom_fichier):
    if os.path.isfile(nom_fichier):
        print('isfile')
        piece = open(nom_fichier, "rb")
        part = MIMEBase('application', 'octet-stream')
        part.set_payload((piece).read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', "piece; filename= %s" % os.path.basename(nom_fichier))
        msg.attach(part)
    else:
      print(f'{nom_fichier} is not a file')

def sendMail(mail, subject, text, files=[]):
    msg = MIMEMultipart()
    msg['From'] = 'radiogaga' + '@obs-nancay.fr'
    msg['To'] = mail
    msg['Subject'] = subject
    msg.attach(MIMEText(text))
    print(files,len(files))
    if (len(files) > 0):
        for ifile in range(len(files)):
            attach_file(msg, files[ifile])
            #print(files[ifile])
    mailserver = smtplib.SMTP('smtp.obs-nancay.fr')
    # mailserver.set_debuglevel(1)
    mailserver.sendmail(msg['From'], msg['To'].split(','), msg.as_string())
    mailserver.quit()
    print('Send a mail: \"%s\"" to %s' % (subject, mail))


HOME_DIR = Path('/home/radiogaga/')
RADIOGAGA_PYTHON_DIR = HOME_DIR / 'radiogaga-python'
#RADIOGAGA_PYTHON_DIR = HOME_DIR / 'Nancay/Nenufar/RadioGAGA/script_test/radiogaga/'
#OBS_TO_PROCESS_TXT = '/data/nenuFAR/RadioGAGa/obs_to_process.txt'
PYTHON_CMD = '/home/radiogaga/anaconda3/bin/python'
DATA_TAMPON_DIR = Path('/data/nenuFAR/')
DATA_DIR        = Path('/data/nenuFAR//RadioGAGa')
OBS_TO_PROCESS_TXT = DATA_DIR / 'obs_to_process.txt'

TIMEOUT         = 2 # Minutes


# setting log file
logging.basicConfig(filename=f"{HOME_DIR}/pipeline.log",
                    filemode='a',
                    level=logging.INFO,
                    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")
logger = logging.getLogger()
logger.warning('Restart service')

while (True):

  # Check for actual obs to process
  with open(OBS_TO_PROCESS_TXT,'r') as f:
    obs_dirs = f.readlines()
    #print(obs_dirs)
    if len(obs_dirs) > 0 and obs_dirs[0]:
      s_actual_OBS = obs_dirs[0]
      s_actual_OBS = s_actual_OBS.replace('\n','')
      split = s_actual_OBS.split(" ")
      if len(split) > 1:
        s_actual_OBS = split[0]
        arg = split[1]
      else: 
        arg = ''

      b_OBS_running = True
      if arg == 'force':
        force = True
      else: 
        force = False
      print(s_actual_OBS,force)
    else:
      s_actual_OBS = ''
      b_OBS_running = False
     
  sleep(1)
  if b_OBS_running:
    logger.info(f'A new observation is running  !')
    logger.info(f'PATH          : {s_actual_OBS}')

   
    ######################
    # init the observation
    ######################

    try:
      parset = Parset(s_actual_OBS + "/current.parset")
    except:
      b_OBS_running = False
      print("No parset found")
      result = os.system(f"sed -i \'1d\' {OBS_TO_PROCESS_TXT}")
      continue

    logger.info(f'ID            : {parset.ObsID}')
    logger.info(f'Name          : {parset.ObsName}')
    logger.info(f'ObsStartTime  : {parset.ObsStartTime}')
    logger.info(f'ObsStopTime   : {parset.ObsStopTime}')
    logger.info(f'ObsTopic      : {parset.ObsTopic}')

    #### Move zips from /data/YYYY/MM/DD to /data/Radiogaga/KP/YYYY/MM/OBS_NAME 
    directory_start = DATA_TAMPON_DIR / f"{parset.ObsStartTime.year}" / f"{parset.ObsStartTime.month:02d}" / f"{parset.ObsStartTime.day:02d}"
    directory_stop = DATA_TAMPON_DIR / f"{parset.ObsStopTime.year}" / f"{parset.ObsStopTime.month:02d}" / f"{parset.ObsStopTime.day:02d}"

    start = int( f"{parset.ObsStartTime.hour:02d}" + f"{parset.ObsStartTime.minute:02d}"+ f"{parset.ObsStartTime.second:02d}")
    stop = int( f"{parset.ObsStopTime.hour:02d}" + f"{parset.ObsStopTime.minute:02d}"+ f"{parset.ObsStopTime.second:02d}")

    logger.debug(f'directory_start : {directory_start}')
    logger.debug(f'directory_stop : {directory_stop}')
    logger.debug(f'start : {start}')
    logger.debug(f'stop : {stop}')
    logger.info(f'The pipeline is ready !')
    logger.info('Waiting for incoming events...')
    #date = datetime.now()
    # Fix 
    # Init a date in the past
    # C'est ne pas rater 2 obs à la suite. 
    # Si le traitment de l'obs numéro 1 fini aprés la fin de l'obs numéro 2 
    # Alors l'obs numéro 2 ne sera pas traité
    # C'est rare mais sur des petites obs de tests avec beaucoup d'événement ca arrive ...
    date = datetime(2020,1,1,) 
    last_date_zip_received = date
    # Test
    #end_date = date + timedelta(minutes=10,seconds=0)
    end_date = parset.ObsStopTime
    print(date,end_date)
    print(date < end_date)
    no_new_events = True

    ############################
    # Now starting the main loop
    ############################
    while (b_OBS_running  and date < end_date) or not no_new_events or (force and b_OBS_running) : # and no zips received since X minutes
      #print('cest parti')
      date = datetime.now()
      if force :
         no_new_events = True


      #### Move zips from /data/YYYY/MM/DD to /data/Radiogaga/KP/YYYY/MM/OBS_NAME 
      files_to_move = []
      if directory_start == directory_stop:
          zips = glob(f"{directory_start}/*.zip")
          zips.sort()
          #print(zips)
          for zip in zips:
              datefile = int(zip.split("-")[1].split("_")[0])
              logger.debug(datefile)
              if start <= datefile and datefile <= stop:
                no_new_events = True
                files_to_move.append(zip)
      else :
          # Obs à cheval sur 2 jours
          # Veille
          zips = glob(f"{directory_start}/*.zip")
          zips.sort()
          #print(zips)     
          for zip in zips:
              datefile = int(zip.split("-")[1].split("_")[0])
              if start <= datefile  and datefile< 235959:
                  no_new_events = True
                  files_to_move.append(zip)
          
          # Jour J 
          zips = glob(f"{directory_stop}/*.zip")
          
          zips.sort()
          for zip in zips:
              datefile = int(zip.split("-")[1].split("_")[0])
              if 0 <= datefile and datefile <= stop:
                  files_to_move.append(zip)
                  no_new_events = True
      #print(files_to_move)
      # Cache new events copied fo run pipeline on them 

      if len(files_to_move) > 0 :
        logger.info(f'Receiving {len(files_to_move)} new events')
        with open(f'{HOME_DIR}/new_events.txt','w+') as f:
          f.write('name\n') # Column name
          for file in files_to_move:
            name = os.path.basename(file)
            name = name.replace('.zip','')
            f.write(name+'\n')
        b_run_pipeline = True
      else:
        b_run_pipeline = False

      if force:
        b_run_pipeline = True


      try:
          for file in files_to_move:
              os.rename(file, DATA_DIR / s_actual_OBS / os.path.basename(file))
      except OSError as error:
          logger.debug(error)
      
      
      # Run pipeline ! 
      if b_run_pipeline:
        cmd = [PYTHON_CMD,
              RADIOGAGA_PYTHON_DIR / 'scripts/eparse/pipeline.py'
              ]
        args_cmd = ['allmr',
                'far'  ,
                s_actual_OBS,
                '--cache',
                f'--selected={HOME_DIR}/new_events.txt',
                '--db',
                '--repointing',
                '--processors=8'
                ]
        
        if force:
           args_cmd.remove(f'--selected={HOME_DIR}/new_events.txt')

        command = cmd + args_cmd
        print(b_run_pipeline,command)
        proc = subprocess.Popen(command)
        logger.debug(f'Run pipeline with PID : {proc.pid}')
        logger.debug(f'Processing currents events, waiting end ...')
        rc = proc.wait()
        if rc != 0:
          logger.error(f'Processing end with error, rc : {rc}')
        
        # Clean events to process
        with open(f'{HOME_DIR}/new_events.txt','w+') as f:
          f.write('name\n') # Column name

        

      # Timeout for accumulated events on NB0 machine
      if datetime.now() > end_date and not force:
        if no_new_events: 
          no_new_events = False
          last_date_zip_received = datetime.now()
          #logger.info(f'Observation {parset.ObsName} is finished')
          logger.info(f'Obs finished, set TIMEOUT of {TIMEOUT} minutes to receive last events from NB0')
        delta = datetime.now() - last_date_zip_received
        #print(delta,type(delta),delta.seconds/60)
        logger.info(f'Waiting for last events... TIMEOUT = {round(TIMEOUT - delta.seconds/60,2)} minutes')
        if (delta.seconds / 60) > TIMEOUT:
          logger.info('TIMEOUT ! ')
          no_new_events = True
        else:
          sleep(30)     

      if force:
        force = False
        b_OBS_running = False
        no_new_events = True 

      sleep(1)


    #################
    # End of OBS    
    

    # Delete the line in OBS_TO_PROCESS_TXT
    result = os.system(f"sed -i \'1d\' {OBS_TO_PROCESS_TXT}")
    if result == 0: 
      logger.debug(f'The line was succesfully deleted in {OBS_TO_PROCESS_TXT}') 
#
    logger.info(f'Start post processing, for plot selected events and send mail')
    #################
    # query the database to get all events of the observation in the dataframe
    df = db.query_between_start_end("radiogaga_events", parset.ObsStartTime, parset.ObsStopTime)
    if "no_events" in df : 
      no_events = True
    else :
      no_events = False

    if not no_events : 
      # Some statistics
      n_events  = len(df)
      n_pointage = len(df[df.is_ana_pointing == 'True'])
      n_true_events = len(df[df.is_ana_pointing == 'False'])

      #Columns: [FPGA_ver, MR_used, SN_x, SN_y, alt_event, alt_source, az_event, az_source, heatmap_coherent, heatmap_ratio, is_ana_pointing, 
      # max, name, noise_level, polar_trigged, pos_x, pos_y, risging_time, source, upd_count]

      ## Query to select "positive" events
      degree = 10
      SN_r = 3
      # Comment your query to print into the resume for the mail 
      commented_query = f"{degree} degree around alt/az pointing and SN_r sould be >= to {SN_r}"
      ### your query
      query  = f'alt_event > (alt_source - {degree})'
      query += f' and alt_event < (alt_source + {degree})'
      query += f' and  az_event > az_source - {degree}'
      query += f' and az_event < az_source + {degree}'
      query += f' and (SN_x > {SN_r} or SN_y > {SN_r}) '
      df_selected = df.query(query)
      df_selected['name'].to_csv(f'{HOME_DIR}/pipeline_selected.txt') 
      n_selected_events = len(df_selected)

      # Get the "best" event of selected to plot it in .png and send it by mail
      if n_selected_events > 0:
        # Get the max
        best_event = df_selected.query('SN_x == SN_x.max()')['name']
        best_event_str = best_event.to_string(index=False)

        best_event.to_csv(f'{HOME_DIR}/best_event.txt') 
        print("!!!!!")
        print(s_actual_OBS)
        # Run pipeline to get picture of the best event
        logger.info('Process best event ...')
        cmd = [PYTHON_CMD,
              RADIOGAGA_PYTHON_DIR / 'scripts/eparse/pipeline.py'
              ]
        args = ['allmr',
                'far'  ,
                s_actual_OBS,
                '--cache',
                f'--selected={HOME_DIR}/best_event.txt',
                '--jpg',
                '--no-db',
                '--repointing'
                ]
        command = cmd + args
        proc = subprocess.Popen(command)
        rc = proc.wait()
        logger.info(f'Process best event is finished with rc = {rc}')

      # Run pipeline to process PDF of selected events
      logger.info('Process selected events ...')
      cmd = [PYTHON_CMD,
            RADIOGAGA_PYTHON_DIR / 'scripts/eparse/pipeline.py'
            ]
      args = ['allmr',
              'far'  ,
              s_actual_OBS,
              '--cache',
              f'--selected={HOME_DIR}/pipeline_selected.txt',
              '--pdf',
              '--no-db',
              '--repointing'
              ]
      command = cmd + args
      proc = subprocess.Popen(command)
      rc = proc.wait()
      logger.info(f'Process selected events is finished with rc = {rc}')

      # body to send the mail
      message = f"""An observation is finished : 
  ID            : {parset.ObsID}
  Name          : {parset.ObsName}
  ObsStartTime  : {parset.ObsStartTime}
  ObsStopTime   : {parset.ObsStopTime}
  ObsTopic      : {parset.ObsTopic}
  PATH          : {s_actual_OBS}


  Number of events                   : {n_events}
  Number of analogic pointing events : {n_pointage}
  Number of true events              : {n_true_events}
  --------------------
  Number of selected events          : {n_selected_events}
  -> {commented_query}
      """
      if n_selected_events > 0:
        message += f"Attached here is the best event during the observation :\n"
        files = [f'{s_actual_OBS}/output/images/{best_event_str}.png']
      else:
        files = []
      KP = parset.ObsTopic.split(' ')[0]
      subject =f"{KP} {parset.ObsName} is finished"
    
    else:
      files = []
      message = message = f"""An observation is finished : 
  ID            : {parset.ObsID}
  Name          : {parset.ObsName}
  ObsStartTime  : {parset.ObsStartTime}
  ObsStopTime   : {parset.ObsStopTime}
  ObsTopic      : {parset.ObsTopic}
  PATH          : {s_actual_OBS}
  """
      subject =f"WARNING ! {parset.KP} {parset.ObsName} is finished ! NO EVENTS"

    to = 'thomas.berthet@obs-nancay.fr'
    to += ',lilian.martin@subatech.in2p3.fr'
    to += ',richard.dallier@subatech.in2p3.fr'
    to += ',valentin.decoene@subatech.in2p3.fr'
    sendMail(to,subject,message,files=files)

    ###### END ! 
    logger.info(f'Observation {parset.ObsName} is finished ! ')
    logger.info(f'Waiting for a new observation...')
    b_OBS_running = False
    force = False
    s_actual_OBS = ''
    ###### Ready for a new loop ! 

  


 
    


      


    



  
    
