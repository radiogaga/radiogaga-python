import os
import logging

# CONSTANTS
LOCAL_DIR = '/home/berthet/import/'
CODALEMA_HOST = "codadaqsa2"

logging.basicConfig(filename=f'{LOCAL_DIR}pipeline.log',filemode='a', level=logging.DEBUG,format='%(asctime)s - %(levelname)s - %(message)s')

result = os.popen(f'ssh {CODALEMA_HOST} cat /data/nenuFAR/RadioGAGa/obs_to_process.txt').read()

#if result != 0:
#  logging.error(f'code : {result}, Can\' access to {CODALEMA_HOST} ')
#  exit()
# transform result of the files into a list
obs_dirs = names_list = [y for y in (x.strip() for x in result.splitlines()) if y] 
logging.info(f'Found {len(obs_dirs)} observations to process')
result = os.system(f'ssh {CODALEMA_HOST} "sed -i \'1d\' /data/nenuFAR/RadioGAGa/obs_to_process.txt"')
for obs in obs_dirs:
  
  if obs[-1] == '/':
    obs = obs[:-1] # Remove last '/'
  obs_basename = obs.split('/')[-1]

  result = os.system(f'rsync -r {CODALEMA_HOST}:{obs} {LOCAL_DIR}')
  if result != 0:
    logging.error(f'code : {result}, something goes wrong during importing observation directory {obs} ')
    exit()

  logging.info(f'Observation directory {obs} successfully imported')
  logging.info('Starting pipeline...')
  result = os.system(f'/home/berthet/Nancay/Nenufar/RadioGAGA/script_test/radiogaga/venv/bin/python /home/berthet/Nancay/Nenufar/RadioGAGA/script_test/radiogaga/scripts/eparse/pipeline.py new_round_pattern far {LOCAL_DIR+obs_basename}/')
  logging.info(result)
  if result != 0:
    logging.error(f'code : {result}, something goes wrong during pipeline')
    exit()
  logging.info(f'Pipeline finished')
  result = os.system(f'rsync -r {LOCAL_DIR+obs_basename}/output {CODALEMA_HOST}:{obs}/')
  if result != 0:
    logging.error(f'code : {result}, something goes wrong export output dir {CODALEMA_HOST}:{obs}/')
    exit()
  logging.info(f'Ouput directory {LOCAL_DIR+obs_basename} successfully exported to {CODALEMA_HOST}:{obs} ')






  
