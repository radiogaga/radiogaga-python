### TODO
### Script pour post processer tous les événements passés
### A finir

from glob import glob
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('path', help='Path or the obs directorie to process ex :/home/berthet/tmp/traitement/ES12/08/20220826_030000_20220826_110000_CRAB_TRACKING/')

args = parser.parse_args()

obs = glob(f'{args.path}/*/*/*/*/')
n_obs = len(obs)

  
print(*obs,sep='\n')
print(f'{n_obs} found in {args.path}')

total_zips = 0
with open('past_obs.txt','w') as f:
  for observation in obs:
    zips = glob(f'{observation}/*.zip')
    n_zips = len(zips)
    print(f'{n_zips} zips found in {observation}')
    total_zips += n_zips
    f.write(f'{observation} force\n')

print(f'{total_zips} zips found')