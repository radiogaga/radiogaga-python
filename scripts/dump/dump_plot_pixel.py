
import context

from radiogaga import TBBurst_lib as tbb
from radiogaga import pixel as pix  
from radiogaga import near_field as nf
from radiogaga import rfi_multi as rfi
import numpy as np
import argparse
import matplotlib.pyplot as plt

if __name__ == "__main__":

  
  conf = tbb.load_yaml()
  # Arg parse
  parser = argparse.ArgumentParser(description='Convert tbb_dump.bin in Spectrogram')
  parser.add_argument('path', help='Path')
  parser.add_argument('point_par_pixel',choices=('1000','2000','4000','16000','64000'))
  parser.add_argument('--mr')
  parser.add_argument('--pol')

  args = parser.parse_args()
  point_par_pixel = int(args.point_par_pixel)
  tbb.init_output_dirs(args.path)

  pixel = pix.Pixel(args.path,point_par_pixel)
  print(pixel.mr_list)
  print(pixel.stats.shape)
  




  
  if args.mr is not None and args.pol is not None :
    def update_temp_plot(n0,ax4,ax5):
      #plt.clf()
      ax4.clear()
      ax5.clear()
      rfi.plot_temp_axe(pixel.files,n0,point_par_pixel,ax4,ax5)
      plt.draw()
      

    def mouse_event(event):
      x = event.xdata.round(3)
      y = event.ydata.round(3)
      print(pixel.scalex,pixel.scaley)
      x_pix = x   * pixel.H // pixel.scalex
      y_pix = y * pixel.H // pixel.scaley
      n0 = y_pix * point_par_pixel * pixel.H + x_pix * point_par_pixel
      n0 = int(n0)
      print(f'x: {x} and y: {y} , n0 : {n0}')
      print(x_pix,y_pix)
      update_temp_plot(n0,ax4,ax5)
      

    # Plot the MR/polar selected
    mr = int(args.mr)
    pol = int(args.pol)
    fig = plt.figure(figsize=(19, 10))
    ax1 = plt.subplot2grid(shape=(4, 4), loc=(0, 0), colspan=2,rowspan=4)
    #ax2 = plt.subplot2grid(shape=(4, 4), loc=(2, 0), colspan=2)
    #ax3 = plt.subplot2grid(shape=(4, 4), loc=(3, 0), colspan=2)
    ax4 = plt.subplot2grid(shape=(4, 4), loc=(0, 2), rowspan=4)
    ax5 = plt.subplot2grid(shape=(4, 4), loc=(0, 3), rowspan=4)

    cid = fig.canvas.mpl_connect('button_press_event', mouse_event)
    board,chan = tbb.get_board_chan_from_mr(pixel.mr_list,mr)
    np.savetxt(f"{args.path}/output/txt/stats_{point_par_pixel}.csv",pixel.stats['PAYLOAD'][:,:,board,chan,pol],'%0d')
    print(board,chan)
    
    pixel.plot_axe(board,chan,pol,ax1)
    #plt.savefig(f"{args.path}/test_{point_par_pixel}.png",dpi=1200)
    plt.show()
    plt.draw()

    

  else:
    # Else run the function to plot pdf
    pixel.plot_pdf()
