
import context

from radiogaga import TBBurst_lib as tbb
from radiogaga import pixel as pix  
from radiogaga import near_field as nf
from radiogaga import rfi_multi as rfi
import numpy as np
import argparse
import matplotlib.pyplot as plt

if __name__ == "__main__":

  
  conf = tbb.load_yaml()
  # Arg parse
  parser = argparse.ArgumentParser(description='Plot trigged pixels')
  parser.add_argument('path', help='Path')
  parser.add_argument('point_par_pixel',choices=('1000','4000','16000','64000'))
  parser.add_argument('seuil')
  parser.add_argument('--mr')
  parser.add_argument('--pol')

  args = parser.parse_args()
  point_par_pixel = int(args.point_par_pixel)
  tbb.init_output_dirs(args.path)

  pixel = pix.Pixel(args.path,point_par_pixel)

  if args.mr is not None and args.pol is not None :
    hot_points,shift, MR = pixel.trig_hot_points(int(args.seuil),int(args.mr),int(args.pol))
  else:
    hot_points,shift, MR = pixel.trig_hot_points(int(args.seuil))

  n_hot_points = len(hot_points)
  print(f"{n_hot_points} detected on {MR}")
  
  fig = plt.figure()
  for idx in range(n_hot_points):
    
    #ax1 = plt.subplot2grid(shape=(4, 4), loc=(0, 0), colspan=2,rowspan=4)
    #ax2 = plt.subplot2grid(shape=(4, 4), loc=(2, 0), colspan=2)
    #ax3 = plt.subplot2grid(shape=(4, 4), loc=(3, 0), colspan=2)
    ax4 = plt.subplot2grid(shape=(4, 4), loc=(0, 2), rowspan=4)
    ax5 = plt.subplot2grid(shape=(4, 4), loc=(0, 3), rowspan=4)
    print(hot_points[idx])
    n0 = hot_points[idx] * point_par_pixel# - shift[idx] 
    rfi.plot_temp_axe(pixel.files,n0,point_par_pixel,ax4,ax5)
    plt.show()
  