
import context

from radiogaga import TBBurst_lib as tbb
from radiogaga import pixel  
from radiogaga import near_field as nf
from radiogaga import rfi_multi as rfi



import argparse


if __name__ == "__main__":
  board = 0
  chan = 3
  polar = 0

  ma_list = (28, 29, 30, 31, 32, 33, 34, 35)

  yaml = tbb.load_yaml()

  # Arg parse
  parser = argparse.ArgumentParser(description='Convert tbb_dump.bin in Spectrogram')
  parser.add_argument('path', help='Path')
  args = parser.parse_args()
  # Load .bin
  files, files_name = tbb.load_dump(args.path)
  # To test
  #files = files[:2]
  #files_name = files_name[:2]
  nof_files = len(files)
  n_pixel = 16000 
  stats,process = pixel.init_output_var(args.path,nof_files,n_pixel)
  
  if process:
    pixel.run_process(files,stats,processes=1)

  if True:
    hot_points,shift, MR = pixel.trig_hot_points(stats,200,files)
    print(f"{len(hot_points)} detected on {MR}")
    pixel.export_hot_points_txt(files,hot_points,shift,ma_list,yaml,args.path) 