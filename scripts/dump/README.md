# Dump de 5 sec

Pour utiliser ces scripts ils est fortement conseillé d'être sur un **nancep**. Les fichiers traités sont très volumineux et et les traitements long.

Ensuite une fois sur un noeud nancep, il faut placé le résultat du dump avec tous les .bin dans un même dossier dans /data/myusername. Pour cela on peut faire de la façon suivante : 

**Sur un nancep**
```bash
mkdir /data/myusername/mydump
rsync -aP  tberthet@tbb:/data/mydump/ /data/myusername/mydump/
```
Une fois cela fait tous les script se lance en paramètre le chemin ou se trouve le dump

## 1. dump_plot_pixel.py

Permet de générer un PDF avec une page par MR d'une vue **pixel**.
![pixel](../../images/pixel.png)

```
usage: dump_plot_pixel.py [-h] [--mr MR] [--pol POL] path {1000,2000,4000,16000,64000}
```

```bash
# To generate PDF
python dump_plot_pixel.py /data/tberthet/dump_0505 2000
# To plot one channel and allow to click on pixels to plot temporal view
python dump_plot_pixel.py /data/tberthet/dump_0505 2000 --mr 37 --pol 1

```

## 2. dump_plot_multi.py
Permet de plotter les vues temporelles de chaque **hot_points**.
```
usage: dump_plot_multi.py [-h] [--mr MR] [--pol POL] path {1000,4000,16000,64000} seuil
```

```bash
# The program choose the MR with the most detection 
python dump_plot_multi.py /data/tberthet/dump_0505/ 1000 200 
# We can specified the MR and the polar wich will be process to trig hot points
python dump_plot_multi.py /data/tberthet/dump_0505/ 1000 200 --mr 37 --pol 
```



## 3. dump_resume_rfi.py
!!! Attention ne marche plus avec les modification du module pixel. A corriger TODO ! 
```
python3 dump_resume_rfi.py /data/mon_dump/
```
!!! warning
    Il faut avoir générer le PDF **pixel** avant de lancer ce script pour récupérer les points chauds.

A l'aide de tous les points chauds récupérés, un pdf est généré avec un points chaud par page avec un nearfield , une vue spectrale et temporelle. 

![resume_rfi](../../images/resume_rfi.png)



## 4. dump_export_hot_points_as_txt.py

```
python3 dump_export_hot_points_as_txt.py /data/mon_dump/
```

Exporter les formes d'ondes des points chauds de quelques MR en .txt afin de nourrir des simus FPGA pour tester le trigger de RadioGaGA.

TODO : 
- Ajouter la possibilité d'ajouter les délais si le dump a été fait pendant une observation


