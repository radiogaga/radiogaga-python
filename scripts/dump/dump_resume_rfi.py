#!/usr/bin/python3
from matplotlib.backends.backend_pdf import PdfPages
from tqdm import tqdm
import context

from radiogaga import TBBurst_lib as tbb
from radiogaga import pixel as pix  
from radiogaga import near_field as nf
from radiogaga import rfi_multi as rfi
from radiogaga import pointing
import numpy as np
import argparse
import matplotlib.pyplot as plt

if __name__ == "__main__":

  
  conf = tbb.load_yaml()
  # Arg parse
  parser = argparse.ArgumentParser(description='Plot trigged pixels')
  parser.add_argument("mode",choices=(('near,far')),help="Mode of field view")
  parser.add_argument('path', help='Path')
  parser.add_argument('point_par_pixel',choices=('1000','4000','16000','64000'))
  parser.add_argument('seuil')
  parser.add_argument('--mr')
  parser.add_argument('--pol')

  args = parser.parse_args()
  point_par_pixel = int(args.point_par_pixel)
  tbb.init_output_dirs(args.path)

  pixel = pix.Pixel(args.path,point_par_pixel)

  if args.mr is not None and args.pol is not None :
    hot_points,shift, MR = pixel.trig_hot_points(int(args.seuil),int(args.mr),int(args.pol))
  else:
    hot_points,shift, MR = pixel.trig_hot_points(int(args.seuil))

  n_hot_points = len(hot_points)
  print(f"{n_hot_points} detected on {MR}")



  if args.mode == 'near':
    mode = nf.Mode.NEAR
  elif args.mode == 'far':
    mode = nf.Mode.FAR

  mr_used = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79,101)

  
  field = nf.nearfield(mode,mr_used)
  
  with PdfPages(f'{args.path}/output/pdf/resume_rfi.pdf') as pdf:
    fig,ax0 = plt.subplots(figsize=(19, 10))

    fig.suptitle(f"Pixel image from MR{MR}, {n_hot_points} hot points ( > {int(args.seuil)}) detected")
    board,chan = tbb.get_board_chan_from_mr(pixel.mr_list,int(args.mr))
    im0 = pixel.plot_axe(board,chan,int(args.pol),ax0)
    fig.colorbar(im0)
    #plt.show()
    pdf.savefig(fig)

    for hot_idx in tqdm(range(n_hot_points)):     
      fig = plt.figure(figsize=(19, 10))

      ax1 = plt.subplot2grid(shape=(4, 4), loc=(0, 0), colspan=2,rowspan=2)
      ax2 = plt.subplot2grid(shape=(4, 4), loc=(2, 0), colspan=2)
      ax3 = plt.subplot2grid(shape=(4, 4), loc=(3, 0), colspan=2)
      ax4 = plt.subplot2grid(shape=(4, 4), loc=(0, 2), rowspan=4)
      ax5 = plt.subplot2grid(shape=(4, 4), loc=(0, 3), rowspan=4)

      n0 = hot_points[hot_idx] * point_par_pixel #- shift[hot_idx]
      #n0 += point_par_pixel//2 - n_point//2
      occured_time = n0 * 5e-9 

      fig.suptitle(f"Hot point n°{hot_idx} occured at {occured_time:.6f} seconde ")   
      n_point = point_par_pixel
      n0 = hot_points[hot_idx] * point_par_pixel - shift[hot_idx]
      n0 += point_par_pixel//2 - n_point//2
  
      # Create temporal view
      rfi.plot_temp_axe(pixel.files,n0,point_par_pixel,ax4,ax5)

      heat_map,max,min = field.process_heat_map(pixel.files,n0,n_point,8)
      field.plot(heat_map,max,min,ax1)



      fig.tight_layout(pad=3.0)
      ax1.set_rasterized(True)
      pdf.savefig(fig)
      
      #print ("\033[A\033[A")
      #print ("\033[A\033[A")
      #plt.show()
      #plt.savefig(f"{hot_idx}.png",dpi=400)

print("Processing finished !")


