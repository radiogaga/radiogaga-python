import matplotlib.pyplot as plt
import argparse
import numpy as np
import os
from glob import glob

import context
from radiogaga import TBBurst_lib as tbb
import radiogaga.rfi_multi as rfi
from radiogaga import pointing
from radiogaga import near_field as nf

parser = argparse.ArgumentParser(description='Plot all MR of an eparse trig')
parser.add_argument('path', help='Path')
args = parser.parse_args()

zips = glob(f"{args.path}/*.zip")
zips.sort()
tbb.init_output_dirs(args.path)


lmn = pointing.loag_altazB(args.path)
pointing.plot_3d_view(lmn)

input()