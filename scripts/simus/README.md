# Simulations de pointage numérique


Ici ces scrips permettent d'avoir une idée de la réponse du pointage numérique en fonction du pointage de la source, d'un pattern de MR choisis sur le backend RadioGaGA. 

## Utilisation
Il faut passer en paramètre, un pattern de MR selon la liste suivante, ainsin que le alt az de la source pointée.
TODO : placer les pattern dans un package qu'on import ca serait plus propre

``` 
Digital pointing simulation

positional arguments:
  {allmr,actual_pattern,round_pattern,ns_pattern,ne_pattern,nw_pattern,cross_pattern}
                        Altitude of the pointed source
  alt                   Altitude of the pointed source
  az                    Altitude of the pointed source

optional arguments:
  -h, --help            show this help message and exit

```
``` bash
python3 simu_pointing.py round_pattern 70 180
# ou
python3 simu_pointing_3d.py round_pattern 70 180
```

## Résultat

### simu_pointing.py
![input](../../images/simu_input_1d.png)
![input](../../images/simu_output_1d.png)

### simu_pointing_3d.py
![input](../../images/simu_input_3d.png)
![input](../../images/simu_output_3d.png)