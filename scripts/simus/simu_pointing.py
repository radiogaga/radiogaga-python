import argparse
import numpy as np
import matplotlib.pyplot  as plt

import context
from radiogaga import pointing
from radiogaga import TBBurst_lib as tbb

board_cfg = tbb.load_yaml()

pattern = pointing.pattern

parser = argparse.ArgumentParser(description='Digital pointing simulation')
parser.add_argument("pattern",choices=pattern.keys(),help="Altitude of the pointed source")
parser.add_argument("alt",help="Altitude of the pointed source")
parser.add_argument("az",help="Altitude of the pointed source")


args = parser.parse_args()
print(args)
if __name__ == '__main__':

  ma_list = pattern[args.pattern]
  n_MR = len(ma_list)

  positions = tbb.load_MR_position(ma_list)

  # Source pointing
  #az = 90 
  #alt = 45 
  az_pointing = float(args.az)
  alt_pointing = float(args.alt)

  lmn_pointing = pointing.alt_az_to_lmn(alt_pointing,az_pointing)

  # Events
  alt = 0
  n_event = 360
  lmn_events = np.zeros((n_event,3))
  az_events = np.arange(0,360,360/n_event)
  for idx,az in enumerate(az_events):
    lmn_events[idx,:] = pointing.alt_az_to_lmn(alt,az)  

  # Transposition des positions des MR sur un vecteur entre 0-1 pour rentrer dans le plot
  positions_1 = (positions - (pointing.LOFAR_center)) /400 # 400 = diamètre de Nenufar en mètre
  pointing.plot_3d_view(lmn_pointing,lmn_events,positions_1)

  # Calcul des delais du pointage de la source
  delais_pointing = np.zeros((len(positions)))
  for idx,pos in enumerate(positions):
    delai = pointing.get_delay_from_lmn(pos,lmn_pointing)
    delais_pointing[idx] = delai
  
  # Calcul des delais des evenements
  delais_events= np.zeros((n_event,n_MR))
  for idx_lmn, lmn in enumerate(lmn_events):
    for idx_pos,pos in enumerate(positions):
      #print(f'LMN : {lmn }MR {idx} - delai = {delai}')
      delai = pointing.get_delay_from_lmn(pos,tuple(lmn))
      delais_events[idx_lmn,idx_pos] = delai
  
  # Difference des delais
  delais_diff =  delais_pointing - delais_events 

  # Statistique
  sigma = np.std(delais_diff,axis=1) #/ 256
  var = np.var(delais_diff,axis=1)   #/ 256 
  print("sigma",sigma)
  #print("var",var)

  # Plot courbes des delais
  fig = plt.figure()
  ax = plt.axes()
  ax.plot(delais_pointing,label="pointing")
  ax.plot(delais_events[int(az_pointing),:],label="event")
  ax.plot(delais_diff[int(az_pointing),:],label="diff")
  plt.xticks(range(len(ma_list)),ma_list)
  plt.legend()

  if False:
    # Plot courbes des delais 3d
    fig = plt.figure()
    ax = plt.axes(projection='3d')
    x, y = np.meshgrid(range(n_MR),range(n_event))
    ax.plot_wireframe(x,y,delais_pointing[np.newaxis,:],rstride=10, cstride=10,color = 'blue', label="pointing")
    ax.plot_wireframe(x,y,delais_events,rstride=10, cstride=10,color = 'orange', label="event")
    ax.plot_wireframe(x,y,delais_diff,rstride=10, cstride=10,color = 'green', label="diff")
    plt.xticks(range(len(ma_list)),ma_list)
    plt.legend()

  # Plot polar
  fig = plt.figure()
  fig.suptitle(f'Ecart type des délais en fonction de l\'azimuth \n pour un pointage à alt: {alt_pointing}° az: {az_pointing}°')
  ax = plt.axes(projection='polar')
  ax.set_theta_zero_location("N")
  ax.set_theta_direction(-1)
  az_events = az_events /180 * np.pi 
  ax.fill(az_events, sigma)
  ax.set_ylim(0,50)
  plt.tight_layout()
  plt.show()





