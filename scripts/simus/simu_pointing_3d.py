from cProfile import label
import math
import context

import argparse
import os
import numpy as np
import matplotlib.pyplot  as plt
from radiogaga import pointing
from radiogaga import TBBurst_lib as tbb

board_cfg = tbb.load_yaml()

pattern = pointing.pattern

parser = argparse.ArgumentParser(description='Digital pointing simulation')
parser.add_argument("pattern",choices=pattern.keys(),help="Altitude of the pointed source")
parser.add_argument("alt",help="Altitude of the pointed source")
parser.add_argument("az",help="Altitude of the pointed source")


args = parser.parse_args()
print(args)
if __name__ == '__main__':

  ma_list = pattern[args.pattern]
  n_MR = len(ma_list)

  positions = tbb.load_MR_position(ma_list)

  # Source pointing
  #az = 90 
  #alt = 45 
  az_pointing = float(args.az)
  alt_pointing = float(args.alt)

  lmn_pointing = pointing.alt_az_to_lmn(alt_pointing,az_pointing)


  # Events
  n_az = 36
  n_alt = 18  
  lmn_events = np.zeros((n_alt,n_az,3))
  alts = np.arange(0,180,180/n_alt)
  azs = np.arange(0,360,360/n_az)
  for idx_alt,alt in enumerate(alts):
    for idx_az, az in enumerate(azs):
      lmn_events[idx_alt,idx_az,:] = pointing.alt_az_to_lmn(alt,az)  

  # Transposition des positions des MR sur un vecteur entre 0-1 pour rentrer dans le plot
  positions_1 = (positions - (pointing.LOFAR_center)) /400 # 400 = diamètre de Nenufar en mètre
  pointing.plot_3d_view(lmn_pointing,lmn_events,positions_1)

  # Calcul des delais du pointage de la source
  delais_pointing = np.zeros((len(positions)))
  for idx,pos in enumerate(positions):
    delai = pointing.get_delay_from_lmn(pos,lmn_pointing)
    delais_pointing[idx] = delai
  
  # Calcul des delais des evenements
  delais_events= np.zeros((n_alt,n_az,n_MR))
  for idx_alt,alt in enumerate(alts):
    for idx_az, az in enumerate(azs):
      for idx_pos,pos in enumerate(positions):
        #print(f'LMN : {lmn }MR {idx} - delai = {delai}')
        delai = pointing.get_delay_from_lmn(pos,tuple(lmn_events[idx_alt,idx_az,:]))
        delais_events[idx_alt,idx_az,idx_pos] = delai
  
  # Difference des delais
  delais_diff =  delais_pointing - delais_events  


  # Plot courbes
  #fig = plt.figure()
  #ax = plt.axes()
  #ax.plot(delais_pointing,label="pointing")
  #ax.plot(delais_events[9,:],label="event")
  #ax.plot(delais_diff[9,:],label="diff")
  #plt.xticks(range(len(ma_list)),ma_list)
  #plt.legend()

  #print(delais_diff.shape)
 ## delais_diff.shape = (18*36,n_MR)
  #print(delais_diff.shape)
  #print(*delais_diff,sep='\n')
  sigma = np.std(delais_diff,axis=2)
  var = np.var(delais_diff,axis=2)
  #sigma.shape = (18,36)
  #var.shape = (18,36)
  print("sigma",sigma)
  #print("var",var)

  lmn = np.zeros((n_alt,n_az,3))
  for idx_alt,alt in enumerate(alts):
    for idx_az, az in enumerate(azs):
      lmn[idx_alt,idx_az,:]= pointing.alt_az_to_lmn(idx_alt*10,idx_az*10,sigma[idx_alt,idx_az])

  fig = plt.figure()
  ax = plt.axes(projection='3d')
  ax.plot_wireframe(lmn[...,0],lmn[...,1],lmn[...,2], rstride=1, cstride=1,color = 'blue')
  ax.text(50,0,0,'E')
  ax.text(-50,0,0,'W')
  ax.text(0,50,0,'N')
  ax.text(0,-50,0,'S')
  ax.set_xlim(-50,50)
  ax.set_ylim(-50,50)
  ax.set_zlim(0,50)
  #plt.tight_layout()

  #fig = plt.figure()
  #ax = plt.axes(projection='polar')
  #ax.set_theta_zero_location("N")
  #ra_event_radian = ra_events /180 * np.pi 
  #ax.plot(ra_event_radian, var)
  ##ax.set_theta_direction(-1)
  #plt.fill_between(ra_event_radian,var)
  plt.show()




