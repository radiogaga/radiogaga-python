
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import glob

import argparse
import os
from tqdm import tqdm
from yaml import parse
import random
                    

import context

from radiogaga import TBBurst_lib as tbb
from radiogaga import pixel  
from radiogaga import near_field as nf
from radiogaga import rfi_multi as rfi
from radiogaga import pointing

"""
TODO : 
Ajouter la position de pointage
Ajuster tailler spectre/nearfield

"""

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Plot all MR of an eparse trig')
  parser.add_argument("mode",choices=('near,far'),help="Mode of field view")
  parser.add_argument('path', help='Path')
  parser.add_argument('processes', help='N of processes')
  parser.add_argument('pdf', help='N of processes')
  parser.add_argument('--nofevents')
  args = parser.parse_args()

  zips = glob.glob(f"{args.path}/*.zip")
  n_zips = len(zips)
  if args.nofevents is not None and int(args.nofevents) < n_zips:
    random_events = random.sample(range(1, n_zips), int(args.nofevents))
    print(random_events)
    zips = np.array(zips)[random_events]
    print(zips)


  zips.sort()
  tbb.init_output_dirs(args.path)

  
  allmr = list(pointing.pattern['allmr_ext'])
  print(allmr)
  allmr.remove(100)
 
  positions_all = tbb.load_MR_position(pointing.pattern['allmr'])
  #board_cfg = tbb.load_yaml()
  #MR_used = []
  #for board in board_cfg.values():
  #  MRs = [b for b in board['MR'] if b != -1]
  #  MR_used += MRs
  #MR_used.sort()
  #MR_used.remove(101)
  #MR_used.remove(102)
  #print(MR_used)

  if args.mode == 'near':
    mode = nf.Mode.NEAR
  elif args.mode == 'far':
    mode = nf.Mode.FAR

  field = nf.nearfield(mode,positions_all)
  list_az = "#name alt az x y\n"
  try:
    fig = plt.figure(figsize=(19, 10))
    with PdfPages(f'{args.path}/output/pdf/resume_rfi_{mode}.pdf') as pdf:
        for z in tqdm(zips):
          files, info, parameters, processed = tbb.load_eparse_zip(z)
          n_ech = files[0].shape[0]
         
          date,time,udp_cnt = tbb.parse_eparse_name(os.path.basename(z))
          lmn = pointing.get_lmn_from_datetime(args.path,date,time)
          alt_source, az_source = pointing.lmn_to_alt_az(lmn)
          name = os.path.basename(z).replace(".zip","")
        

          ax1 = plt.subplot2grid(shape=(4, 4), loc=(0, 0), colspan=2,rowspan=4)
          #ax2 = plt.subplot2grid(shape=(4, 4), loc=(2, 0), colspan=2)
          #ax3 = plt.subplot2grid(shape=(4, 4), loc=(3, 0), colspan=2)
          ax4 = plt.subplot2grid(shape=(4, 4), loc=(0, 2), rowspan=4)
          ax5 = plt.subplot2grid(shape=(4, 4), loc=(0, 3), rowspan=4)


          #plt.show()
          
          #pixel.plot_axe(stats,board,chan,polar,ax1,nof_files)
        
          n0 = 0
          n_point = 2000
          # Create spectrae view
          #rfi.plot_fft_axe(files,n0,n_point,ax2,ax3)
          # Create temporal view
          rfi.plot_temp_axe(files,n0,n_point,ax4,ax5)

          file = f'{args.path}/output/bin/heat_map_{name}_{args.mode}.npz'
          if not os.path.exists(f"{file}"):
            heat_map,max,min = field.process_heat_map(files,n0,n_point,int(args.processes))
            with open(f'{file}','wb') as f:
              np.save(f,heat_map)
          else: 
              heat_map = np.load(file)
              max = heat_map.max()
              min = heat_map.min()

          x,y,*_ = np.where(heat_map == max)
          x ,y = x[0],y[0]
          alt,az = pointing.x_y_to_alt_az(x,y,mode)
          fig.suptitle(f"date : {date}  time : {time}  UDP count : {udp_cnt} | pointage  alt :  {round(alt_source,2)}° az : {round(az_source,2)}° | event alt : {round(alt,2)}° az : {round(az,2)}° | utc : {info.utc_nano} | gps : {info.gps_nano} | tbb : {info.tbb_nano}")
          if args.pdf =='1':
            field.plot(heat_map,max,min,ax1,lmn)

          fig.tight_layout(pad=3.0)
          ax1.set_rasterized(True)
          list_az += f"{name} {alt} {az} {x} {y}\n"
          if args.pdf =='1':
            pdf.savefig(fig)
          #savefig(fig,f"{args.path}/output/images/{name}.png")
          #plt.imsave(fig,f"{args.path}/output/images/{name}.pdf")
          
          #plt.show()
  except KeyboardInterrupt:
    print("Interrupted by user")
    pass

  with open(f'{args.path}/output/list_az_{args.mode}.txt','w') as f:
    f.write(list_az)





