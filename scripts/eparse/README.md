# Trig eparse enregistré sur codadaqsa2

Les scripts ici n'ont pas de recommandations particulières pour leur exécution.  

Ils permettent de traiter des trig eparse au format .zip présents sur codadaqsa2.
Pour cela il suffit de selectionner les **.zip** souhaités, les placés dans un dossier sur sa machine locale et dans lancés les scripts ci dessous avec en argument le chemin de ce dossier.

## 4. pipeline.py

```
python pipeline.py new_round_pattern far /home/berthet/tmp/20220818_110000_20220818_125800_SOLAR_WIND-J1022+1001_2022-08-18/
```
Script principal qui génère cette vue : 
![Pipeline](../../images/pipeline.png)


## 2. eparse_resume_rfi.py
```
python3 eparse_resume_rfi.py /data/mon_dump/
```

Un pdf est généré avec un trig par page avec un nearfield , une vue spectrale et temporelle. 

![resume_rfi](../../images/resume_rfi.png)

## 3. eparse_plot_multi.py
```
python3 plot_pixel.py /data/dir_of_bin/
```

Permet de générer un PDF avec une vue temporelle de tous les MR pour chaque trig.


## 4. eparse_export_txt.py
```
python3 export_hot_points_as_txt.py /data/dir_of_bin/
```

Exporter les formes d'ondes des trig des MR souhaités en .txt afin de nourrir des simus FPGA pour tester le trigger de RadioGaGA.
