from glob import glob
import argparse
import numpy as np
import os
import glob
from tqdm import tqdm


import context

from radiogaga import TBBurst_lib as tbb
import radiogaga.rfi_multi as rfi
from radiogaga import pointing





parser = argparse.ArgumentParser(description='Plot all MR of an eparse trig')
parser.add_argument("pattern",choices=pointing.pattern.keys(),help="Altitude of the pointed source")
parser.add_argument('path', help='Path')
parser.add_argument("--alt",help="Altitude of the pointed source")
parser.add_argument("--az",help="Altitude of the pointed source")
args = parser.parse_args()

zips = glob.glob(f"{args.path}/*.zip")
zips.sort()


tbb.init_output_dirs(args.path)


ma_list = pointing.pattern[args.pattern]
n_MR = len(ma_list)
list_files = ""
positions = tbb.load_MR_position(ma_list)


for z in tqdm(zips):
    files,infos,parameters,processeds = tbb.load_eparse_zip(z)
    n_ech = files[0].shape[0]
    
    data = np.zeros(shape=(n_ech,n_MR*2))
    yaml =tbb.load_yaml()

    for idx,mymr in enumerate(ma_list):
        board,chan = tbb.get_board_chan_from_mr(yaml,mymr)
        #print(files[0].shape)
    #    print(files[board].shape)
        data[:,idx*2] = files[board][:,chan*2]
        data[:,idx*2+1] = files[board][:,chan*2+1]

    date, time, _ = tbb.parse_eparse_name(os.path.basename(z))
    lmn = pointing.get_lmn_from_datetime(args.path,date,time)
    if args.alt is not None and args.az is not None:
      lmn = pointing.alt_az_to_lmn(float(args.alt),float(args.az))
    #lmn = pointing.alt_az_to_lmn(int(args.alt),int(args.az))
    delays_s = ""
    for mr_pos in positions:
      #print(mrpos)
      delay = pointing.get_delay_from_lmn(mr_pos,lmn)
      delays_s += " " + str(delay)     
    
    name = os.path.basename(z)
    name = name.replace(".zip","")
    udp_cnt = name.split("_")[1]
    udp_cntfill = (10-len(udp_cnt)) * "0" + udp_cnt # Fill udp_cnt with zero to have always same len
    name = name.replace("_"+ udp_cnt, "_" + udp_cntfill)
    
    name = name + ".csv"
    name_delays = name + delays_s
    list_files += name_delays +"\n"

    np.savetxt(f"{args.path}/output/txt/{name}",data,delimiter=" ",fmt="%d")

with open(f"{args.path}/output/txt/list.txt",'w') as f:
  f.write(list_files)

    
    
    
    #data=data['data'].ravel()
    #data = data.reshape((-1,8))
    #dest = file.replace(".dat","")
    #dest = dest.replace("/","_")
    #dest = dest.replace("data_","")
    #print(f"Save in {args.output}/{dest}")
    #dest = (58-len(dest)) * "_" + dest
    #numpy.savetxt(f"{args.output}/{dest}.txt",data,delimiter=" ",fmt="%d")





