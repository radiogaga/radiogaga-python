#! /usr/bin/python3
import matplotlib.pyplot as plt
import glob
import argparse
import numpy as np
import os
from tqdm import tqdm
from matplotlib.backends.backend_pdf import PdfPages

import context

from radiogaga import TBBurst_lib as tbb
from radiogaga import pointing
from radiogaga import trigger as trig
import radiogaga.rfi_multi as rfi
board_cfg = tbb.load_yaml()

MR_used = pointing.pattern['allmr']

positions = tbb.load_MR_position(MR_used)

parser = argparse.ArgumentParser(description='Plot all MR of an eparse trig')
parser.add_argument('path', help='Path')
args = parser.parse_args()

zips = glob.glob(f"{args.path}/*.zip")
zips.sort()


tbb.init_output_dirs(args.path)

fig = plt.figure(figsize=(19, 10))


with PdfPages(f'{args.path}/output/pdf/eparse.pdf') as pdf:
  for z in tqdm(zips):
    files,infos,parameters,processed = tbb.load_eparse_zip(z)
    
    n_ech = files[0].shape[0]
    ax0 = plt.subplot2grid(shape=(1, 2), loc=(0, 0))
    ax1 = plt.subplot2grid(shape=(1, 2), loc=(0, 1))

    date,time,udp_cnt = tbb.parse_eparse_name(os.path.basename(z))
    lmn = pointing.get_lmn_from_datetime(args.path,date,time)
  
    fig.suptitle(f"date : {date}  time : {time}  UDP count : {udp_cnt} ")
    #n_point = data.shape[0]
    allmr_trig = trig.Trigger(files,lmn,pointing.pattern['allmr'],positions)
    files_phased = allmr_trig.np_to_file(allmr_trig.data_phased_filtred)
    rfi.plot_temp_axe(files_phased,0,n_ech,ax0,ax1)
    plt.tight_layout()
    pdf.savefig()
    name = os.path.basename(z).replace(".zip","")
    #plt.savefig(f"{args.path}/output/images/{name}.svg")
    #
      #plt.show()



