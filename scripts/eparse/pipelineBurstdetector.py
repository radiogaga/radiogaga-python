#! /usr/bin/python3
# Script principal pour le traiment des événements radiogaga
# python pipeline -h pour avoir de l'aide sur les arguments
# ex : python pipeline.py new_round_pattern far /home/berthet/tmp/20220818_110000_20220818_125800_SOLAR_WIND-J1022+1001_2022-08-18/


from turtle import pos
from unittest import result
import matplotlib.pyplot as plt
from glob import glob
import argparse
import numpy as np
import os
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib import animation
from tqdm import tqdm
from datetime import datetime,timezone
from contextlib import nullcontext
import time
import pandas as pd
import context

from radiogaga import TBBurst_lib as tbb
import radiogaga.rfi_multi as plot_multi
from radiogaga import pointing
from radiogaga import near_field as field
from radiogaga import trigger as trig
from radiogaga.database import Database
from radiogaga.parset import Parset
from radiogaga.altaz import Altaz

# Connect to database
import env
db = Database(env.HOST_DB,8086,env.USER_DB,env.PSWD_DB,'nenufar')
list_pos = ""
# Argparse
parser = argparse.ArgumentParser()
parser.add_argument("pattern",choices=pointing.pattern.keys(),help="Altitude of the pointed source")
parser.add_argument("field_mode",choices=(('near','far')),help="Mode of field view")
parser.add_argument('path', help='Path or the obs directorie to process ex :/home/berthet/tmp/traitement/ES12/08/20220826_030000_20220826_110000_CRAB_TRACKING/ ')
parser.add_argument("--fps",help="fps of video",default=10)
parser.add_argument("--alt",help="Altitude of the pointed source")
parser.add_argument("--az",help="Altitude of the pointed source")
parser.add_argument('--cache', action='store_true',help="Cache result or near/far field")
parser.add_argument('--no-cache', dest='cache', action='store_false',help="No result for near/far field")
parser.add_argument('--pdf', action='store_true',help="pdf output enable")
parser.add_argument('--no-pdf', dest='pdf',action='store_false',help="pdf output disable")
parser.add_argument('--mp4', action='store_true',help="mp4 output enable")
parser.add_argument('--no-mp4', dest='mp4',action='store_false',help="mp4 output disable")
parser.add_argument('--show', action='store_true',help="Plot.show()")
parser.add_argument('--db', action='store_true',help="db output enable")
parser.add_argument('--no-db', dest='db',action='store_false',help="db output disable")
parser.add_argument('--selected',dest='selected',help='.csv with selected events')
parser.add_argument('--processors',dest='processors',help='Nof processors to process field view',default=8)
parser.add_argument('--repointing',dest="repointing",action="store_true",default=False,help='Enable to repoint in field result direction')
parser.add_argument('--nplot',dest='nplot',help='N first events to plot')

args = parser.parse_args()
# Get zips to process
zips = glob(f"{args.path}/*.zip")
zips.sort()
# Init output directory in path
tbb.init_output_dirs(args.path)
if args.nplot is not None:
  zips = zips[:int(args.nplot)]
# Get MR patterns
ma_list_all = pointing.pattern[args.pattern]
# Event pos 
event_pos = 1024
OVERSAMPLE = 1

# Position
nof_MR_all = len(ma_list_all)
positions_all = tbb.load_MR_position(pointing.pattern[args.pattern])
# Field mode
if args.field_mode == 'near':
  mode = field.Mode.NEAR
elif args.field_mode == 'far':
  mode = field.Mode.FAR
field = field.nearfield(mode,positions_all,OVERSAMPLE,cache=args.cache,fov=180,processors=int(args.processors))

# Selected events
print(args.selected)
if args.selected is not None:
  df = pd.read_csv(args.selected)
  print(df)



#Video
if args.mp4:
  FFMpegWriter = animation.writers['ffmpeg']
  metadata = dict(title='Movie Test', artist='Matplotlib',
                  comment='Movie support!')
  writer = FFMpegWriter(fps=int(args.fps), metadata=metadata)



# Read parset


# Read altaz
try:
  altazA = Altaz(args.path+"/current.altazA")
  altazB = Altaz(args.path+"/current.altazB")
  parset = Parset(args.path+"/current.parset")
  no_parset = False
except:
  no_parset = True
# Create 4K plot
fig = plt.figure(figsize=(39, 20))

# null context to create conditional with
#https://stackoverflow.com/questions/27803059/conditional-with-statement-in-python
with writer.saving(fig, f'{args.path}/output/pipeline_{args.field_mode}.mp4', 100) if args.mp4 else nullcontext() : 
  with PdfPages(f'{args.path}/output/pdf/pipeline_{args.field_mode}.pdf') if args.pdf else nullcontext() as pdf:
    for idx,z in enumerate(tqdm(zips)):
      name = os.path.basename(z).replace(".zip","")
      if args.selected is not None:
        if df['name'].isin([name]).any():
          process =True
        else:
          process = False
      else:
        process = True

      if process:
        #if df.where('name'==name)
        #################
        #
        ##################
        starttime= time.time()#$$$
        date,temps,udp_cnt = tbb.parse_eparse_name(os.path.basename(z))
        # Get files (data), and different paramters and processeed values
        files, infos, parameters,processed = tbb.load_eparse_zip(z)     
        time_event = datetime.fromtimestamp(infos.gps_second + infos.gps_nano / 1e9,tz=timezone.utc)
        # Associed the triggedpola
        # TODO
        polar_trigged = 0
        

        n_ech = files[0].shape[0]
        function = "Open file"#$$$
        print(f'Exec time {function} : {time.time() - starttime } s')#$$$

        starttime= time.time()#$$$
        # Get pointing
        if no_parset: # Can be manually choose for debug
          if args.alt is not None and args.az is not None:
            alt_source,az_source = int(args.alt), int(args.az)
          else:
            alt_source,az_source = 90, 0
          lmn = pointing.alt_az_to_lmn(alt_source,az_source)
          is_ana_pointing = False
        else:
          alt_source, az_source, lmn = altazB.get_pointing_from_datetime(time_event)
          # Check if the event it's due to an analog pointing
          is_ana_pointing = altazA.check_if_ana_pointing(time_event)     
        
        
        
        allmr_trig = trig.Trigger(files,lmn,ma_list_all,positions_all,parameters.filters_path,event_pos)
        allmr_oversampled_trig = trig.Trigger(files,lmn,ma_list_all,positions_all,parameters.filters_path,event_pos,oversample=OVERSAMPLE)
        files_filtred = allmr_trig.np_to_file(allmr_trig)

        
        if mode == field.mode.FAR:
          n_point =n_ech
          data_for_heat_map = np.zeros((n_ech,nof_MR_all,2))
          data_for_heat_map[0:n_ech,:,:] = allmr_trig.data_filtred_std_mad[(event_pos)-(n_point)//2:(event_pos)+(n_point)//2,:,:]
          #data_for_heat_map[:,:,:] = allmr_oversampled_trig.data_filtred[(event_pos*OVERSAMPLE)-(n_point)//2:(event_pos*OVERSAMPLE)+(n_point)//2,:,:]
          #data_for_heat_map[:,:,:] = allmr_oversampled_trig.derive_data[(event_pos*OVERSAMPLE)-(n_point)//2:(event_pos*OVERSAMPLE)+(n_point)//2,:,:]
          
        elif mode == field.mode.NEAR:
          n_point = n_ech
          data_for_heat_map = np.zeros((n_ech,nof_MR_all,2))
          data_for_heat_map[0:n_ech,:,:] = allmr_trig.data_filtred_std_mad[event_pos-n_point//2:event_pos+n_point//2,:,:]


        max_mean = np.mean(np.max(data_for_heat_map,axis=0))
        print('mean',max_mean)

        
        name = os.path.basename(z).replace(".zip","")

        function = "Processing"#$$$
        print(f'Exec time {function} : {time.time() - starttime } s')#$$$
        
        starttime= time.time()#$$$  

        heatamp_coherent = False
        heatmap_ratio = 0
        repointed = False
        alt = 0
        az = 0
        x = 0
        y = 0

        if not is_ana_pointing:
          heat_map,max,min, result_pos = field.process_heat_map(data_for_heat_map[:,:,polar_trigged],path=args.path,name=name)
          mean = np.mean(heat_map)
          print(result_pos)

          heatmap_ratio = max/mean 

          if mode == field.mode.FAR:
            alt,az = result_pos
            lmn_scanned = pointing.alt_az_to_lmn(alt,az)
            if args.repointing:
              allmr_trig = trig.Trigger(files,lmn_scanned,ma_list_all,positions_all,parameters.filters_path,event_pos)
              allmr_oversampled_trig_repointed = trig.Trigger(files,lmn_scanned,ma_list_all,positions_all,parameters.filters_path,event_pos,oversample=OVERSAMPLE)
              repointed = True
            else : 
              repointed = False
          elif mode == field.mode.NEAR:
            x,y = result_pos
            data_phased = np.zeros(data_for_heat_map.shape)
            delays = field.delays_no_reshape[x,y,0,:]
            for chan in range(data_phased.shape[1]):
              delay = delays[chan]
              #print(delay)
              allmr_trig.data_phased_filtred[:,chan,0] = np.roll(allmr_trig.data_filtred[:,chan,0],delay)
              allmr_trig.data_phased_filtred[:,chan,1] = np.roll(allmr_trig.data_filtred[:,chan,1],delay)
            
            


        function = "Field"#$$$
        print(f'Exec time {function} : {time.time() - starttime } s')#$$$

            #heat_map,max,min = field.process_heat_map(data_for_heat_map[:,:,polar_trigged].copy(order='F'),0,0,8)

        
        #################
        # Database insert
        #################
#        if args.db:
#          starttime= time.time()#$$$
#          tags = {"source": f'{parset.ObsTopic.split(" ")[0]}/{parset.ObsName}',
#                  "polar_trigged": polar_trigged,
#                  "FPGA_ver":infos.FPGA_ver,
#                  "MR_used":ma_list,
#                  "is_ana_pointing":is_ana_pointing
#              }
#
#          fields = {
#                "name":name,
#                "upd_count":infos.cnt,
#                "risging_time": float(processed.rising_time[polar_trigged] ),
#                "max": float(processed.max[polar_trigged]),
#                "noise_level": float(processed.level_bruit[polar_trigged]),
#                "SN_x":float(radiogaga_mr_trig.SN[0]),
#                "SN_y":float(radiogaga_mr_trig.SN[1]),
#                "alt_source": float(alt_source),
#                "az_source": float(az_source),
#                'alt_event' : float(alt),
#                'az_event' : float(az),
#                'pos_x' : float(x),
#                'pos_y' : float(y),
#                'heatmap_ratio' : float(heatmap_ratio),
#                'heatmap_coherent': bool(heatamp_coherent)
#              }
#          
#          db.insert_event(time_event,measurement="TBB_events",tags=tags,fields=fields)
#          db.push_events()
#          function = "Database insert"#$$$
#          print(f'Exec time {function} : {time.time() - starttime } s')#$$$
        with open(f'{args.path}/output/list_pos_{args.field_mode  }.txt','w') as f:
            f.write(list_pos)
        list_pos += f"{name} {x} {y}\n"
        ################
        ## Plotting 
        ################
        plt.rcParams["figure.figsize"] = (39,20)

        # Plot name of the DIR on the left size
        if (args.pdf or args.mp4 or args.show) and not is_ana_pointing:
          if args.path[-1] == '/':
            name = args.path.split('/')[-2]
          else :
            name = args.path.split('/')[-1]
          plt.figtext(0.07,0.5,name,fontsize=30,rotation="vertical",ha="center",va="center",weight='bold')

          plt.suptitle(f"date : {date}  time : {temps}  UDP count : {udp_cnt} | Pointing : alt : {alt_source} az : {az_source}",fontsize=20)
          
          # Left column
          ax0_3 = plt.subplot2grid(shape=(6, 6), loc=(0, 0), rowspan=6,)
          ax0_4 = plt.subplot2grid(shape=(6, 6), loc=(0, 1), rowspan=6,sharex=ax0_3)

          # midle column
          ax1_0 = plt.subplot2grid(shape=(6, 6), loc=(0, 2), colspan=2,rowspan=3)
          ax1_1 = plt.subplot2grid(shape=(6, 6), loc=(3, 2), )
          ax1_2 = plt.subplot2grid(shape=(6, 6), loc=(3, 3), )
          ax1_3 = plt.subplot2grid(shape=(6, 6), loc=(4, 3), )
          ax1_5 = plt.subplot2grid(shape=(6, 6), loc=(4, 2), )
          ax1_4 = plt.subplot2grid(shape=(6, 6), loc=(5, 2), colspan=2)

          # right column
          
          ax2_3 = plt.subplot2grid(shape=(6, 6), loc=(0, 4), rowspan=6,sharex=ax0_3)
          ax2_4 = plt.subplot2grid(shape=(6, 6), loc=(0, 5), rowspan=6,sharex=ax0_3)
            

          ######################################################
          # left column
          ######################################################


          # Filter
          #n_point = 500
          #ax0_0.set_title('Reconstitution with RadioGaGa MAs')
          #p0 = ax0_0.plot(radiogaga_mr_trig.data_phased_filtred_sommed[event_pos-n_point//2:event_pos+n_point//2])
          ##p0 = ax0_0.plot(radiogaga_mr_trig.data_phased_filtred_sommed_std[event_pos-n_point//2:event_pos+n_point//2])
          ##p0 = ax0_0.plot(allmr_trig.data_phased_filtred_sommed[event_pos-n_point//2:event_pos+n_point//2])
          #ax0_0.legend(p0,('filter X','filter Y'))
          ## Pow
          #p0 = ax0_1.plot(radiogaga_mr_trig.P[event_pos-n_point//2:event_pos+n_point//2,0],label='Pow X')
          #p0 = ax0_1.plot(radiogaga_mr_trig.P[event_pos-n_point//2:event_pos+n_point//2,1],label='Pow X')
          #ax0_1.axhline(y = parameters.level_bruit_max, color = 'red', linestyle = ':',label='level_bruit_max')
          #if processed.level_bruit[0] != 0 :
          #  ax0_1.axhline(y = processed.level_bruit[0], color = 'blue', linestyle = ':',label='level_bruit_x')
          #if processed.level_bruit[1] != 0 :
          #  ax0_1.axhline(y = processed.level_bruit[1], color = 'orange', linestyle = ':',label='level_bruit_y')
          #ax0_1.legend()
          ## Derive
          #p0 = ax0_2.plot(radiogaga_mr_trig.derive[event_pos-n_point//2:event_pos+n_point//2,0],label='Derive X')
          #p0 = ax0_2.plot(radiogaga_mr_trig.derive[event_pos-n_point//2:event_pos+n_point//2,1],label='Derive Y')
          #ax0_2.axhline(y = parameters.k1[0], color = 'r', linestyle = ':',label='K')
          ##ax0_2.legend(p0,('Derive X','Derive Y','k','k1'))
          #ax0_2.legend()
          
          mean_max_data = np.mean(np.max(allmr_trig.data_filtred,axis=0))
          plot_multi.plot_temp_axe_data(allmr_trig.data_filtred[event_pos-n_point//2:event_pos+n_point//2,:,0],ma_list_all,mean_max_data,1,ax0_3)
          plot_multi.plot_temp_axe_data(allmr_trig.data_filtred[event_pos-n_point//2:event_pos+n_point//2,:,1],ma_list_all,mean_max_data,1,ax0_4)
          ax0_3.set_title("Polar X")
          ax0_4.set_title("Polar Y")


          ######################################################
          # middle column
          ######################################################
          # Plotting field far or near
          field.plot(heat_map,max,min,ax1_0,lmn,pos=result_pos)
          # Near to be rasterized for good transparency
          if mode == field.mode.NEAR and args.show==False: 
            ax1_0.set_rasterized(True)
          
          debug = f"""
result_pos : {result_pos}\n


          """
    

          ax1_1.text(0.1,1,"########PARAMETERS#########\n"+parameters.__str__(),verticalalignment='top',horizontalalignment='left',fontsize=12)
          ax1_2.text(0.1,1,"########PROCESSED##########\n"+processed.__str__(),verticalalignment='top',horizontalalignment='left',fontsize=12)
          ax1_3.text(0.1,1,"##########INFOS############\n"+infos.__str__(),verticalalignment='top',horizontalalignment='left',fontsize=12)
          ax1_5.text(0.1,1,"##########DEBUG############\n"+debug,verticalalignment='top',horizontalalignment='left',fontsize=12)
          ax1_1.axis('off')
          ax1_2.axis('off')
          ax1_3.axis('off')
          ax1_5.axis('off')
          ax1_4.plot(allmr_trig.data_phased_filtred_sommed[:,:],linewidth=0.2,)
          ax1_4.axis('off')
         

          ######################################################
          # right column
          ######################################################
          #event_pos = 1000
          n_point = n_ech
          #ax2_0.set_title(f'Reconstitution with "{args.pattern}" MAs, repointed in field result : {repointed}')

          
          #p1 = ax2_0.plot(allmr_trig.data_phased_filtred_sommed[event_pos-n_point//2:event_pos+n_point//2])
          ##p1 = ax2_0.plot(allmr_oversampled_trig_repointed.data_phased_filtred_sommed[(event_pos*OVERSAMPLE)-(n_point*OVERSAMPLE)//2:(event_pos*OVERSAMPLE)+(n_point*OVERSAMPLE)//2])
          ##p1 = ax2_0.plot(allmr_trig.data_phased_filtred_sommed_std[event_pos-n_point//2:event_pos+n_point//2])
          #ax2_0.legend(p1,('filter X','filter Y'))
          ## Pow
          #p1 = ax2_1.plot(allmr_trig.P[event_pos-n_point//2:event_pos+n_point//2,0],label='Pow X')
          #p1 = ax2_1.plot(allmr_trig.P[event_pos-n_point//2:event_pos+n_point//2,1],label='Pow X')
          #ax2_1.axhline(y = parameters.level_bruit_max, color = 'red', linestyle = ':',label='level_bruit_max')
          #if processed.level_bruit[0] != 0 :
          #  ax2_1.axhline(y = processed.level_bruit[0], color = 'blue', linestyle = ':',label='level_bruit_x')
          #if processed.level_bruit[0] != 0 :
          #  ax2_1.axhline(y = processed.level_bruit[0], color = 'orange', linestyle = ':',label='level_bruit_y')
          #ax2_1.legend()
          ## Derive
          #p1 = ax2_2.plot(allmr_trig.derive[event_pos-n_point//2:event_pos+n_point//2,0],label='Derive X')
          #p1 = ax2_2.plot(allmr_trig.derive[event_pos-n_point//2:event_pos+n_point//2,1],label='Derive Y')
          #ax2_2.axhline(y = parameters.k1[0], color = 'r', linestyle = ':',label='K')
          ##ax2_2.legend(p0,('Derive X','Derive Y','k','k1'))
          #ax2_2.legend()

          n_point = n_ech
          #plot_multi.plot_temp_axe_data(allmr_trig.data_phased_filtred_std[(event_pos*OVERSAMPLE)-n_point*OVERSAMPLE//2:(event_pos*OVERSAMPLE)+n_point*OVERSAMPLE//2,:,0],ma_list_all,1000,200,ax2_3)
          #plot_multi.plot_temp_axe_data(allmr_trig.data_phased_filtred_std[(event_pos*OVERSAMPLE)-n_point*OVERSAMPLE//2:(event_pos*OVERSAMPLE)+n_point*OVERSAMPLE//2,:,1],ma_list_all,1000,200,ax2_4)
          #plot_multi.plot_temp_axe_data(allmr_trig.data_phased_filtred_std[event_pos-n_point//2:event_pos+n_point//2,:,0],ma_list_all,1000,200,ax2_3)
          #plot_multi.plot_temp_axe_data(allmr_trig.data_phased_filtred_std[event_pos-n_point//2:event_pos+n_point//2,:,1],ma_list_all,1000,200,ax2_4)
          plot_multi.plot_temp_axe_data(allmr_trig.data_phased_filtred[:,:,0],ma_list_all,mean_max_data,1,ax2_3)
          plot_multi.plot_temp_axe_data(allmr_trig.data_phased_filtred[:,:,1],ma_list_all,mean_max_data,1,ax2_4)
          #plot_multi.plot_temp_axe_data(data_for_heat_map[:,:,0],ma_list_all,mean_max_data,1,ax2_3)
          #plot_multi.plot_temp_axe_data(data_for_heat_map[:,:,1],ma_list_all,mean_max_data,1,ax2_4)
          ax2_3.set_title("Polar X")
          ax2_4.set_title("Polar Y")

          starttime= time.time()#$$$
          if args.show:
            plt.show() 
          else:
            if args.pdf:
              pdf.savefig(fig)
            if args.mp4:
              writer.grab_frame()
          function = "Plotting"#$$$
          print(f'Exec time {function} : {time.time() - starttime } s')#$$$ 
          #plt.savefig(f'{args.path}/output/{date}_{time}_{udp_cnt}.png',dpi=250)
          
        
      

# Push events to database
#db.push_events()

      





  



