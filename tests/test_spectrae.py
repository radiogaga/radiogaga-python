import argparse
from glob import glob
from datetime import datetime,timezone
import matplotlib.pyplot as plt
from sympy import O

import context

from radiogaga import TBBurst_lib as tbb
import radiogaga.rfi_multi as plot_multi
from radiogaga import pointing
from radiogaga import near_field as field
from radiogaga import trigger as trig
from radiogaga.database import Database
from radiogaga.parset import Parset
from radiogaga.altaz import Altaz
from radiogaga.spectrae import Spectrae



# Argparse
parser = argparse.ArgumentParser()
parser.add_argument("pattern",choices=pointing.pattern.keys(),help="Altitude of the pointed source")
parser.add_argument('path', help='Path or the obs directorie to process ex :/home/berthet/tmp/traitement/ES12/08/20220826_030000_20220826_110000_CRAB_TRACKING/ ')
args = parser.parse_args()

zips = glob(f"{args.path}/*.zip")
zips.sort()
tbb.init_output_dirs(args.path)

event_pos = 707
# Read parset
parset = Parset(args.path+"/current.parset")

# Read altaz
args.path+"/current.parset"
try:
  altazA = Altaz(args.path+"/current.altazA")
  altazB = Altaz(args.path+"/current.altazB")
except:
  pass

ma_list_all = pointing.pattern[args.pattern]
positions_all = tbb.load_MR_position(pointing.pattern[args.pattern])




for z in zips:
  files, infos, parameters,processed = tbb.load_eparse_zip(z) 
  time_event = datetime.fromtimestamp(infos.gps_second + infos.gps_nano / 1e9,tz=timezone.utc)
  alt_source, az_source, lmn = altazB.get_pointing_from_datetime(time_event)
  allmr_trig = trig.Trigger(files,lmn,ma_list_all,positions_all,parameters.filters_path,event_pos)

  spectrae = Spectrae(allmr_trig.data_phased_filtred_sommed,Te=5e-9)
  ax1 = plt.subplot(5, 1, 1)
  ax2 = plt.subplot(5, 1, 2)
  ax3 = plt.subplot(5, 1, 3)
  ax4 = plt.subplot(5, 1, 4)
  ax5 = plt.subplot(5, 1, 5)
  spectrae.plot_spectrogram(ax1,polar=0)
  spectrae.plot_spectrogram(ax2,polar=1)
  ax3.plot(allmr_trig.data_phased_filtred_sommed)
  spectrae.plot(ax4,1)
  ax5.plot(spectrae.data[:,1,:])
  plt.show()

