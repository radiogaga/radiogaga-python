
from matplotlib import pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

import context
import numpy as np
import argparse
from glob import glob
import os
from tqdm import tqdm

from radiogaga import TBBurst_lib as tbb
from radiogaga import pixel  
from radiogaga import near_field_class as nf
from radiogaga import rfi_multi as rfi
from radiogaga import pointing



parser = argparse.ArgumentParser(description='Plot all MR of an eparse trig')
parser.add_argument('path', help='Path')
args = parser.parse_args()
path = args.path
path = "0407/"
path = "/home/berthet/tmp/0411_crab"
mr_used = pointing.pattern['allmr']

zips = glob(f"{path}/*.zip")
zips.sort()
print(zips,len(zips))

farfield = nf.nearfield(nf.Mode.FAR,mr_used)
nearfied = nf.nearfield(nf.Mode.NEAR,mr_used)

for z in tqdm(zips):
  fig,(ax0,ax1) = plt.subplots(1,2)
  files = tbb.load_eparse_zip(z)
  date,time,udp_cnt = tbb.parse_eparse_name(os.path.basename(z))
  lmn_crabe = pointing.get_lmn_from_datetime(path,date,time)
  heat_map, max, min = farfield.process_heat_map(files,8,lmn_crabe)

  farfield.plot(heat_map,max,min,ax0)


  heat_map, max, min = nearfied.process_heat_map(files,8)
  nearfied.plot(heat_map, max, min, ax1)
  plt.show()

