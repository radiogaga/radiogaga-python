
import context

import argparse
import os 

from radiogaga import pointing
from radiogaga import TBBurst_lib as tbb

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Convert .dat to csv')
  parser.add_argument("directories", help="File to process",nargs="+")

  args = parser.parse_args()
  ma_list= []
  board_cfg= tbb.load_yaml()

  ma_list = pointing.pattern['allmr']

  positions = tbb.load_MR_position(ma_list)

  directories = args.directories
  #print(*directories,sep='\n')

  path = os.path.dirname(directories[0])
  #date, time, _ = tbb.parse_eparse_name(os.path.basename(directories[7]))
  #lmn = pointing.get_lmn_from_datetime(path,"202204190","140400")
  #print(lmn)

  #lmn = pointing.loag_altazB(path)
  #idx_lmn = 5
  #print(lmn[idx_lmn])
  lmn = pointing.alt_az_to_lmn(45.4,0)
  #lmn =  (-0.000751165631409,  -0.116549773276445,   0.993184567992980)
  
  for idx,mrpos in enumerate(positions):
    delay = pointing.get_delay_from_lmn(mrpos,lmn,te=1e-9)
    print(idx,ma_list[idx],delay -256 ,hex(delay))



