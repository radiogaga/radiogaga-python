import context
from  radiogaga.database import Database

import numpy as np
from datetime import datetime, timedelta
import pandas as pd

import os
from pathlib import Path
import socket
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import smtplib
import subprocess


def attach_file(msg, nom_fichier):
    if os.path.isfile(nom_fichier):
        print('isfile')
        piece = open(nom_fichier, "rb")
        part = MIMEBase('application', 'octet-stream')
        part.set_payload((piece).read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', "piece; filename= %s" % os.path.basename(nom_fichier))
        msg.attach(part)
    else:
      print(f'{nom_fichier} is not a file')

def sendMail(mail, subject, text, files=[]):
    msg = MIMEMultipart()
    msg['From'] = 'radiogaga' + '@obs-nancay.fr'
    msg['To'] = mail
    msg['Subject'] = subject
    msg.attach(MIMEText(text))
    print(files,len(files))
    if (len(files) > 0):
        for ifile in range(len(files)):
            attach_file(msg, files[ifile])
            #print(files[ifile])
    mailserver = smtplib.SMTP('smtp.obs-nancay.fr')
    # mailserver.set_debuglevel(1)
    mailserver.sendmail(msg['From'], msg['To'].split(','), msg.as_string())
    mailserver.quit()
    print('Send a mail: \"%s\"" to %s' % (subject, mail))


import env
db = Database(env.HOST_DB,8086,env.USER_DB,env.PSWD_DB,'nenufar')
#db = Database('localhost',8086,'root','root','radiogaga')

ObsStartTime = '2022-12-06 12:00:00'
ObsStopTime = '2022-12-06 13:55:00'

df = db.query_between_start_end("TBB_events",ObsStartTime,ObsStopTime)

n_events  = len(df)
n_pointage = len(df[df.is_ana_pointing == 'True'])
n_true_events = len(df[df.is_ana_pointing == 'False'])

#Columns: [FPGA_ver, MR_used, SN_x, SN_y, alt_event, alt_source, az_event, az_source, heatmap_coherent, heatmap_ratio, is_ana_pointing, 
# max, name, noise_level, polar_trigged, pos_x, pos_y, risging_time, source, upd_count]
#
degree = 10
heatmap_ratio = 1
# Comment your query to print into the resume for the mail 
commented_query = f"{degree} degree around alt/az pointing and heatmap_ratio sould be >= to {heatmap_ratio}"
### your query
query  = f'alt_event > (alt_source - {degree})'
query += f' and alt_event < (alt_source + {degree})'
query += f' and  az_event > az_source - {degree}'
query += f' and az_event < az_source + {degree}'
query += f' and heatmap_ratio > {heatmap_ratio}'
df_selected = df.query(query)
n_selected_events = len(df_selected)

print(n_selected_events)
if n_selected_events > 0:
  best_event = df_selected.query('heatmap_ratio == heatmap_ratio.max()')['name']
  best_event_str = best_event.to_string(index=False)
  print(best_event)
  print('\n\n')
  print(best_event_str)
  print('\n\n')


  best_event.to_csv('/home/berthet/best_event.txt') # TODO

 # Run pipeline ! 
  PYTHON_CMD = 'python'
  HOME_DIR = Path('/home/berthet/')
  RADIOGAGA_PYTHON_DIR = HOME_DIR / 'Nancay/Nenufar/RadioGAGA/script_test/radiogaga/'
  s_actual_OBS = '/home/berthet/tmp/20221206_120000_20221206_135500_SOLAR_WIND-2022-12-06/'

  cmd = [PYTHON_CMD,
        RADIOGAGA_PYTHON_DIR / 'scripts/eparse/pipeline.py'
        ]
  args = ['allmr',
          'far'  ,
          s_actual_OBS,
          '--cache',
          f'--selected={HOME_DIR}/best_event.txt',
          '--jpg',
          '--no-db'
          ]
  command = cmd + args
  proc = subprocess.Popen(command)
  rc = proc.wait()
  print(rc)

ObsID = '335453'
ObsName = "BS_REOBS_C007A"
ObsTopic = "LT03 PULSARS"
KP = ObsTopic.split(' ')[0]

message = f"""An observation is finished : 
ID            : {ObsID}
Name          : {ObsName}
ObsStartTime  : {ObsStartTime}
ObsStopTime   : {ObsStopTime}
ObsTopic      : {ObsTopic}


Number of events                   : {n_events}
Number of analogic pointing events : {n_pointage}
Number of true events              : {n_true_events}
--------------------
Number of selected events          : {n_selected_events}
-> {commented_query}

Attached here is the best event during the observation : 
"""
subject =f"{KP} {ObsName} is finished"
sendMail('thomas.berthet@obs-nancay.fr',subject,message,files=[f'{s_actual_OBS}output/images/{best_event_str}.png'])
print(f'{s_actual_OBS}/output/images/{best_event_str}.png')

print(message)

