import context
from radiogaga import TBBurst_lib as tbb  

import os
from glob import glob
import argparse
from tqdm import tqdm
from datetime import datetime,timedelta

# Argparse
parser = argparse.ArgumentParser()
parser.add_argument('path', help='Path or the obs directorie to process ex :/home/berthet/tmp/traitement/ES12/08/20220826_030000_20220826_110000_CRAB_TRACKING/ ')
args = parser.parse_args()

zips = glob(f"{args.path}/*.zip")
zips.sort()
output = ""
mean = 0
for z in tqdm(zips[:]):
  date,time,udp_cnt = tbb.parse_eparse_name(os.path.basename(z))
  files, infos, parameters,processed = tbb.load_eparse_zip(z) 
  timestamp = datetime.utcfromtimestamp(infos.gps_second)
  #timestamp = datetime(1980, 1, 6) + timedelta(seconds=int(infos.gps_second) - (35-17 ))
  timefile = datetime.strptime(date+time,"%Y%m%d%H%M%S")
  diff = (timefile - timestamp) 
  line = f"udp_cnt : {infos.cnt} TS : {infos.gps_second} TS_readable : {timestamp}  timefile : {timefile} diff : {diff}"
  #print(line)
  output += line + "\n"
  mean += diff.total_seconds()

mean /= 100
print(output)
print(f'mean : {mean}')