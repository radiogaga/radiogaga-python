from dataclasses import field
import context

from  radiogaga.database import Database

import numpy as np
from datetime import datetime, timedelta
import pandas as pd

db = Database('localhost',8086,'root','root','radiogaga')


#n = 50
#for i in range (n):
#  time = datetime.now()
#  time = time - timedelta(hours=2,milliseconds=n-i*1 )
#  current_time = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
#  noise_level = n*10 - n/5 + n
#
#  max = 1000 * np.cos(2*np.pi*0.1*i) 
#  risging_time = 4 + i//2
#  udp = i
#
#
#  tags = {"source": "CRAB_TRACKING",
#            "polar_trigged": "X",
#            "FPGA_ver":"20220515"
#          }
#  fields = {
#            "upd_count":udp,
#            "risging_time": risging_time,
#            "max": max,
#            "noise_level": noise_level,
#           }
#  db.insert_event(time,'ES12',tags,fields)
#
#db.push_events()

start = datetime(2022,9,12)
fields = '"pos_x","pos_y","udp_count"'
result = db.query_between_start_end("TBB_events",start,fields=fields)
print(result)
