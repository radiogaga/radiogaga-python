
from matplotlib import pyplot as plt

import context
import numpy as np

from radiogaga import TBBurst_lib as tbb
from radiogaga import pixel  
from radiogaga import near_field as nf
from radiogaga import rfi_multi as rfi
from radiogaga import pointing

import os



import glob

n0 = 0
n_point = 2000

path ="0407"
zips = glob.glob(f"{path}/*.zip")
zips.sort()
tbb.init_output_dirs(f"{path}")

mr_used =pointing.pattern['allmr']
positions = tbb.load_MR_position(mr_used)

files = []
lmns = np.zeros((n_point,3))
alt = 63
az = 180
lmn = pointing.alt_az_to_lmn(alt,az)
for board in range(len(mr_used)//4):
  tmp = np.zeros((2000,8))
  for chan in range(4):
    delay = - pointing.get_delay_from_lmn(positions[board*4+chan],lmn)
    print(board*4+chan,delay)
    tmp[delay-1:delay+2,chan*2:chan*2+1] = 100

  files.append(tmp)
print(len(files))
ax0 = plt.axes()
ax1 = plt.axes()

rfi.plot_temp_axe(files,0,2000,ax0,ax1)
plt.show()
file = 'output/bin/delays.npz'
if not os.path.exists(f"{path}/{file}"):
  delays = nf.init_satellite_view_sky(mr_used)
  with open(f'{path}/{file}','wb') as f:
    np.save(f,delays)
else : 
  delays = np.load(f'{path}/{file}')
delays.shape = (-1,len(mr_used))


map_size = (180,180) #pixel 


for z in zips:
  date,time,udp_cnt = tbb.parse_eparse_name(os.path.basename(z))
  lmn_crabe = pointing.get_lmn_from_datetime(path,date,time)

  #files = tbb.load_eparse_zip(z)
  

  nof_MR, P = nf.select_rfi(n0,n_point,files,mr_used)
  P = nf.convolve(nof_MR,P)
  
  heat_map, max_heat, min_heat=nf.apply_delay_mp(P.astype('float64'), delays.astype('int64'), delays.shape[0],map_size)
  #heat_map, max_heat, min_heat=nf.apply_delay_mp(P, delays, delays.shape[0],map_size)


  ax0 = plt.axes()
  my_red_cmap = plt.cm.Reds
  my_red_cmap.set_under(color="white", alpha="0")
  ax0.imshow(heat_map.transpose(),
              origin='upper',
              #vmin=max_heat - (max_heat-min_heat) * 0.2,
              #vmax=max_heat,
              #cmap=my_red_cmap
              )
  ax0.set_aspect('equal')
  for alt in range(0,100,10):
    circle = plt.Circle((90, 90), alt, color='green',fill=False,linewidth=0.5)
    ax0.add_artist(circle)
    if alt > 0:
      ax0.text(90,alt,alt,color='green')


  for az in range(0,360,10):
    rad = (az * np.pi/180) -np.pi/2
    x = 90 * np.cos(rad)
    y = 90* np.sin(rad)
    #ax0.plot(x,y,color='blue',linewidth=0.5)
    line = plt.Line2D( (x+90,90),(y+90,90), color='green',linewidth=0.5)
    ax0.add_artist(line)
    x = 85 * np.cos(rad)
    y = 85* np.sin(rad)
    ax0.text(x+90,y+90,az,color='green')
  # Source
  alt,az = pointing.lmn_to_alt_az(lmn_crabe)
  #alt,az = (10,90)
  az = az -90
  print(alt,az)
  x = (90-alt) * np.cos(az*np.pi/180)
  y = (90-alt) * np.sin(az*np.pi/180)
  ax0.plot(90+x,90+y,marker='x')

  plt.show(block=False)


