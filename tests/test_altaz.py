import context
import argparse
from radiogaga.altaz import Altaz

from datetime import datetime




print("""
########
.altazB
########
""")
altazb = Altaz("altaz/current.altazB")
print(altazb.altaz)
time = datetime(2022,8,24,11,35,25)
beam = 2 
alt,az,lmn = altazb.get_pointing_from_datetime(time)
print(f""" 
time : {time}
beam : {beam}
Alt  : {alt}
Az   : {az}
lmn  : {lmn}
""")
print("""
########
.altazA
########
""")
altaza = Altaz("altaz/current.altazA")
print(altaza.altaz)
print("Ana pointing false")
time = datetime(2022,8,24,11,49,11)
beam = 2 
anapointing = altaza.check_if_ana_pointing(time)
print(anapointing)

print("Ana pointing true")
time = datetime(2022,8,24,11,55,10)
beam = 2 
anapointing = altaza.check_if_ana_pointing(time)
print(anapointing)