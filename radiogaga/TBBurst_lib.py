import glob
import numpy as np
import sys
import os
from datetime import datetime
import yaml
from zipfile import ZipFile
import io
import struct
from radiogaga import frame_parser as fp
import matplotlib.pyplot as plt

ROOT_DIR = os.path.join(os.path.dirname(__file__), '..')



def init_output_dirs(path):
  if os.path.isdir(f"{path}/output/"):
    print("output/ directory already exists")
  else:
    os.mkdir(path+"/output")
    os.mkdir(path+"/output/pdf")
    os.mkdir(path+"/output/bin")
    os.mkdir(path+"/output/images")
    os.mkdir(path+"/output/txt")

def load_dump(path):
  """
  Load dump.bin files located in the passed directory
  """
  N_chan = 8
  conf = load_yaml()
  boards = []

  fullpath = f"{path}/192.168.5.*_dump.bin"
  files = glob.glob(fullpath)


  if len(files) == 0:
    print(f"No dump_files found in {path}")
    exit() 
  else:
    files.sort(key=get_ip_host)
    print(f"{len(files)} files found :")
    files_name = []
    for file in files:
      size = float(format(os.stat(file).st_size/2**30))
      file = os.path.basename(file)
      print(f"{file}      {size} Go")
      files_name.append(file)
    mr_list = files_name_to_MR_list(files_name)

  for idx,file in enumerate(files):
    #idx_board = mr_list[idx]['board']
    boards.append(np.memmap(file, dtype='int16', offset=0, mode='r'))
    boards[idx].shape = (-1, N_chan)
  
  return boards, mr_list

def files_name_to_MR_list(files_name):
  """
  Function to map list of .bin declare in loadd dump with n°MR,rot etc. 
  arg0 : list of files_name of .bin
  """
  conf = load_yaml()
  mr_list = {}
  for idx,name in enumerate(files_name):
    ip =  name.split('_')[0]
    mr_list[idx]= conf[ip]
    mr_list[idx]['board'] = idx
  return mr_list




def load_MR_position(MR_used):
  """
  Load and parse config/MR_Ant10_position.conf
  argument : none
  """
  positions = np.loadtxt(f'{ROOT_DIR}/config/MR_Ant10_position.conf', comments=';')
  positions = {int(p[0]):(p[1],p[2],p[3]) for p in positions}
  positions = [v for k,v in positions.items() if k in MR_used]
  return np.array(positions)
  

def load_yaml():
  """
  Load and parse config/board.yaml
  argument : none
  """
  with open(f'{ROOT_DIR}/config/board.yaml','r') as file:
    try:
      board_cfg = yaml.safe_load(file)
    except yaml.YAMLError as exc:
      print(exc)
      exit(1)
  return board_cfg
  
def get_ip_host(file):
  """
  Get the host IP in the filename passed in arguments
  argument : file
  return : ip host
  ```
  exemple 192.168.5.41_1638975265_dump.bin -> 41
  ```
  """
  file = os.path.basename(file)
  ip = file.split("_")[0].split(".")[3]
  return int(ip)

def get_TS_from_filename(filename):
  """
  Get timestamp in the filemename and return in human readable date format.
  """
  name = os.path.basename(filename)
  ip,TS,_ = name.split("_")
  dt = datetime.fromtimestamp(int(TS))
  date_human = datetime.strftime(dt,"%Y%m%d_%H%M%S")
  return date_human # "YYYYMMDD_hhmmss"



def get_board_chan_from_mr(yaml,mymr):
  """
  Get board number and channel of a MR
  arg1 : pass board.yaml loaded file.
  arg2 : MR list exemple : (12,25,53,68)
  """
  for key,value in yaml.items():
    for chan, mr in enumerate(yaml[key]['MR']):
        #print(board,chan,mr)
        if mr == mymr :
          board = yaml[key]['board']
          result = (board,chan)
          return result
  exit(f"MR {mymr} not found")

def parse_eparse_name(name):
  """
  Return date,time,udp_count from folder name of an eparse trig
  """
  name = name.replace(".zip","")
  datebrut = name.split("-")[0]
  date = datebrut[0:4] + "/" + datebrut[4:6] + "/" + datebrut[6:8]
  tmp = name.split("-")[1]
  timebrut = tmp.split("_")[0]
  time = timebrut[0:2] + ":" + timebrut[2:4] + ":" + timebrut[4:6]
  udp_cnt = tmp.split("_")[1]

  return datebrut,timebrut,udp_cnt

def load_eparse_zip(zip):
  """
  Return list of files 
  arg1: yourzip.zip 
  """
  files=[]
  with ZipFile(zip) as archive:
    #print(archive.namelist())
    for dat in archive.namelist():
      with archive.open(dat) as mydat:
        dat,info,parameters,processed = fp.TBB_read_dat(mydat.read())     
        data=dat['data']#.ravel()
        #plt.plot(data[:,:,0])
        #plt.show()
        data = data.reshape((-1,8)) 
        files.append(data)
    #plt.plot(files[0][:,4])
    #plt.show()
  return files,info, parameters,processed