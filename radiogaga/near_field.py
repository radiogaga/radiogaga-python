import numpy as np
import matplotlib.pyplot as plt


from multiprocessing import Pool
import os
from tqdm import tqdm
import pathlib

import contextily as cx                                                             
import rasterio                           
from rasterio.plot import show as rioshow  
from pyproj import Transformer

from radiogaga import TBBurst_lib as tbb
from radiogaga import pointing
from enum import Enum
import time


def abs2(x):
  return x.real**2 + x.imag**2

# CONSTANTS
Te = 5e-9
Fe = 1/Te
# compute number of spectrae and samples
N_fft = 1024
Nof_acc = 128
c = 299792458.0  # m/s
tof_MR = 22 / c * 1e3
tof_Core = 500 / c * 1e3
polX="NE"
polY="NO"

class Mode(Enum):
  NEAR = 0
  FAR = 1

class nearfield():

  def __init__(self,mode,positions,oversample=1,processors=8,cache=False,fov=180):
    """
    Declare class, with initialise the view for sky, or ground
    arg0: mode(NEAR or FAR)
    arg1 : list of MRs used
    """
    self.mode = mode
    self.positions = positions
    self.nof_MR = len(positions)
    self.oversample = oversample
    self.processors = processors
    self.cache=cache
    self.fov=fov
    if mode == Mode.NEAR:
      self.USN_img, self.USN_ext, self.mesh_size, self.delays, self.west, self.south, self.east, self.north, self.positions = init_satellite_view(positions)
      self.delays_no_reshape = self.delays.copy() # Copy else this variable is reshaped too
      self.delays.shape = (-1,len(positions))
      self.nof_cells = self.delays.shape[0]
    if mode == Mode.FAR:
      self.mesh_size = (180,180)
      #self.mesh_size = (180*5,180*5)
      self.delays = self.init_sky_view(positions)
      self.delays.shape = (-1,len(positions))
  
  def __cache__(self,heat_map,path,name):
    file = f'{path}/output/bin/heat_map_{name}_{self.mode}_{self.mesh_size[0]}.npz'
    
    starttime= time.time()#$$$
    with open(f'{file}','wb') as f:
      np.save(f,heat_map)
    print(f'Exec time cache : {time.time() - starttime } s')#$$$
  def __get_cache__(self,path,name):
    file = f'{path}/output/bin/heat_map_{name}_{self.mode}_{self.mesh_size[0]}.npz'
    if os.path.exists(f"{file}"):
      heat_map = np.load(file)
      max_heat = heat_map.max()
      min_heat = heat_map.min()
      return True, heat_map, max_heat, min_heat
    else:
      return False, None, None, None,



  def process_heat_map(self,datas,path='',name='',n0=0,n_point=0):
    """
    Process heat map
    arg0: datas
    arg1: Path of OBS DIR to pass for caching heat_map
    arg2: Name of event to pass for caching heat_map
    arg3: n0 (Don't used anymore)
    arg4: n_point (Don't used anymore)
    """
    # Check if a cached field result exist
    if self.cache:
      assert path != '' and name != '', "The path of the OBS dir and the name of the event should be passed to cache fields result"
      result,heat_map,max_heat,min_heat = self.__get_cache__(path,name)
      if result:
        pos = self.get_result_position(heat_map,max_heat,min_heat)
        return heat_map, max_heat, min_heat, pos

    # Else make the process
    if type(datas) == np.ndarray: 
      data = datas.copy(order='F').transpose()
      self.P = data
      heat_map, max_heat, min_heat=apply_delay_mp(self.P.astype('float64'), self.delays.astype('int64'), self.delays.shape[0],self.mesh_size,self.mode,self.processors)
      if self.cache:
        assert path != '' and name != '', "The path of the OBS dir and the name of the event should be passed for cache fields result"
        self.__cache__(heat_map,path,name)
      pos = self.get_result_position(heat_map,max_heat,min_heat)
      return heat_map, max_heat, min_heat, pos

    else: # OLD way to do
      nof_MR, self.P = select_rfi(n0,n_point,datas,self.positions)
      heat_map, max_heat, min_heat=apply_delay_mp(self.P.astype('float64'), self.delays.astype('int64'), self.delays.shape[0],self.mesh_size,self.mode,self.processors)

      if self.cache:
        assert path != '' and name != '', "The path of the OBS dir and the name of the event should be passed for cache fields result"
        self.__cache__(heat_map,path,name)
      return heat_map, max_heat, min_heat
  
  def plot(self,heat_map, max_heat, min_heat,ax,lmn_source=(0,0,0),pos=(0,0)):
    if self.mode == Mode.NEAR:
      ax = plot_nearfield(self.USN_img, self.USN_ext, heat_map, max_heat, min_heat, self.west, self.south, self.east, self.north,0, 0,ax,pos)
      return ax
    if self.mode == Mode.FAR:
      im = self.plot_farfield(heat_map, max_heat, min_heat,lmn_source,ax)
      return im

  def init_sky_view(self,positions):
    """
    Initialise the sky view, call by class nearfield
    arg0: list of MRs used
    """
    file = f'{tbb.ROOT_DIR}/cache/delays_{positions[0]}_{self.mesh_size[0]}_{self.oversample}.npz'
    if not os.path.exists(f"{file}") or True: # by passing this, it's useless
      if not os.path.exists(f'{tbb.ROOT_DIR}/cache'):
        os.mkdir(f'{tbb.ROOT_DIR}/cache')

      alts = np.zeros(self.mesh_size)
      azs = np.zeros(self.mesh_size)
      lmns = np.zeros((self.mesh_size[0],self.mesh_size[1],3))
      delays = np.zeros((self.mesh_size[0],self.mesh_size[1],len(positions)))
      pbar = tqdm(total=self.mesh_size[0]*self.mesh_size[1])
      for x in np.arange(self.mesh_size[0]):
        for y in np.arange(self.mesh_size[1]):
          pbar.update(1)
          alts[x,y] = 90-(np.sqrt((x-self.mesh_size[0]//2)**2 + (y-self.mesh_size[0]//2)**2)) /(self.mesh_size[0]//self.fov)
          azs[x,y] = np.arctan2((self.mesh_size[0]/2)-x,(self.mesh_size[0]/2)-y)*(180)/np.pi
          lmns[x,y,:] = pointing.alt_az_to_lmn(alts[x,y],azs[x,y])
          if alts[x,y] > 0:
            #for idx_pos,pos in enumerate(positions):
              delays[x,y,:] = pointing.get_delay_from_lmn(positions,tuple(lmns[x,y,:]),te=5e-9/self.oversample) - 256
              #print(alts[x,y],azs[x,y],delays[x,y])
            #input()
      #with open("az.txt",'wb') as f:
      #  np.savetxt(f,azs,fmt='%.2f')
      #with open("alt.txt",'wb') as f:
      #  np.savetxt(f,alts,fmt='%.2f')
      with open(f'{file}','wb') as f:
        np.save(f,delays)

    else : 
      delays = np.load(f'{file}')
    delays.shape = (-1,len(positions))
    return delays

  def plot_farfield(self,heat_map,max_heat,min_heat,lmn_source,ax):
    #my_red_cmap = plt.cm.Reds
    #my_red_cmap.set_under(color="white", alpha="0")
    alt,az = pointing.lmn_to_alt_az(lmn_source)
    ax.set_aspect('equal') 
    ratio = self.mesh_size[0]//180
    center = self.mesh_size[0]//2

    # Plot elevation circles
    for y in range(0,100*ratio,10*ratio):
      circle = plt.Circle((center, center), y, color='green',fill=False,linewidth=0.5)
      ax.add_artist(circle)
      if alt > 0:
        ax.text(center,y,y//ratio,color='green')

    # PLot azimuth lines
    for az in np.arange(0,360,10):
      rad = (az * np.pi/180) -np.pi/2
      x = center * np.cos(rad)
      y = center * np.sin(rad)
      #ax.plot(x,y,color='blue',linewidth=0.5)
      line = plt.Line2D( (x+center,center),(y+center,center), color='green',linewidth=0.5)
      ax.add_artist(line)
      x = 85 *ratio* np.cos(rad)
      y = 85 *ratio* np.sin(rad)
      ax.text(x+center,y+center,360-az,color='green')
    # Plot Source
    if lmn_source != (0,0,0):
      alt,az = pointing.lmn_to_alt_az(lmn_source)
    ##alt,az = (20,210)
    az = az - 180
    y = (90-alt) * np.cos(az*np.pi/180)
    x = (90-alt) * np.sin(az*np.pi/180)
    ax.plot(center+x,center+y,marker='x',color='red')

    im = ax.imshow(heat_map.transpose(),
                origin='upper',
                vmin=max_heat - (max_heat-min_heat) * 1,
                vmax=max_heat,
                cmap='CMRmap',
                animated=True,
                )
    
    return im


  def get_result_position(self,heat_map,max,min):
    if self.mode == Mode.NEAR:
      # Get 10% of the best point and average it
      x,y,*_ = np.where(heat_map >= max - (max-min) * 0.1)
      
      x = int(np.median(x))
      y = int(np.median(y))
      #x ,y = int(x[0]),int(y[0])
      return (x,y)
    elif self.mode == Mode.FAR:
      # Get the maximum point
      x,y,*_ = np.where(heat_map == max)
      x ,y = int(x[0]),int(y[0])
      alt,az = pointing.x_y_to_alt_az(x,y,self.mode,self.mesh_size)
      return (alt,az)

def init_satellite_view(positions):
  """
  Initialise the satellite view, call by class nearfield
  arg0: list of MRs used
  """
  west, south, east, north, ll = ( 2.17, 47.36, 2.23, 47.39, True)
  west, south, east, north, ll = (  2.17, 47.36, 2.22, 47.39, True)
  #west, south, east, north, ll = ( 2.19, 47.37, 2.22, 47.38, True)

  # Create image with satellite view of USN 
  background_view = pathlib.Path('../aerial.tif')
  if not background_view.exists():
    print("Init USN image, this can take a moment")
    USN_img, USN_ext = cx.bounds2raster(west,
                                      south,
                                      east,
                                      north,
                                      background_view.as_posix(),
                                      ll=ll,
                                      zoom=18,
                                      source=cx.providers.GeoportailFrance.orthos,

                                  #source=cx.providers.OpenStreetMap.France,
                                  )
  with rasterio.open(background_view.as_posix()) as r:
      USN_ext = tuple(r.bounds)
      USN_crs = r.crs
      USN_img = r.read((1,2,3)).transpose((1,2,0))

  #EPSG:3857: « Web Mercator » ou simplement « Mercator » ; code EPSG:3857)
  #EPSG:2154: Lambert 93 (EPSG 2154)
  #EPSG:4326: pWGS84
  transformer = Transformer.from_crs(USN_crs.to_string(), 'EPSG:2154')

  west, south = transformer.transform(USN_ext[0], USN_ext[1])
  east, north = transformer.transform(USN_ext[2], USN_ext[3])
  low, high = 183, 184
  USN_ext = west, east, south, north
  field_bbox = [[west, south, low],[east, north, high]]; dx, dy, dz =  10,  10, 1

  xv, yv, zv = np.ogrid[west:east:dx, south:north:dy, low:high:dz]
  delays = np.sqrt( (xv[..., np.newaxis] - positions[:, 0])**2
                    +(yv[..., np.newaxis] - positions[:, 1])**2
                    +(zv[..., np.newaxis] - positions[:, 2])**2
                    ) * (-1/(c * Te))
  delays = delays.astype('int')
  mesh_size = delays.shape[:-1]
  # Apply delay
  #delays.shape = (-1, len(positions))
  #nof_cells = delays.shape[0]

  return USN_img, USN_ext, mesh_size, delays, west, south, east, north, positions





# Convolve 
def convolve(nof_MR,P):
  N = 5

  for mr in range(nof_MR):
    P[mr,:] = np.convolve(P[mr,:], np.ones(N)/N, mode='same')
  return P
  
def select_rfi(n0,chunk_len,files,positions):
  """
  Select RFI from the chunk of data
  arg0: n0
  arg1: chunk_len
  arg2: files
  arg3: positions
  """
  RFI = [file[n0:n0+chunk_len].transpose() for file in files]
  RFI = np.array(RFI)
  RFI.shape = (-1, 2, chunk_len)
  # selction could be better when we'll add new MRs...
#  positions_np = np.array(positions)
#  print(positions)
#  distant = np.where(positions_np>=100)[0]
#  print(distant)
#  c = 0
#  core = len(positions) - len(distant)
#  for dis in distant:
#    c += 1
#    positions_np[dis] = core + c 
#    print('c',c)
#
#    
#  RFI = RFI[np.r_[positions_np].astype('int'), ...]
#  print(positions_np)
  if len(positions) > 80:
    RFI = RFI[np.r_[0:79:80j, 80, 84].astype('int'), ...]
  else:
    RFI = RFI[np.r_[0:79:80j].astype('int'), ...]
  RFI = RFI.astype('float')
  nof_MR = RFI.shape[0]
  P = (RFI**2).sum(axis=1)
  offset = 20000
  offsets = np.arange(nof_MR) * offset
  baselines = np.median(P, axis=-1)
  P = P - baselines[:, np.newaxis]
  P = convolve(nof_MR, P)
  #P /= P.std(axis=-1)[:, np.newaxis]
  return nof_MR, P




# Heatmap

# field_bbox = [[636915.5785750365, 6695153.549962547, 182], [641931.6840946282, 6699235.287127603, 183]]; dx, dy, dz = 25, 25, 1

from radiogaga.rolls.rolls import rolls_cy

##def apply_delay(P, delays, nof_cells,mesh_size,i=0):
##  """
##  Apply delay, sum it and create heatmap (not used actually)
##  arg0: P
##  arg1: delays
##  arg2: nof_cells
##  arg3: mesh_size
##  """
##  heat_map = np.empty((nof_cells,), dtype='float64')
##  pbar=tqdm(total=nof_cells,position=i,desc=f'process{i}')
##  for idx in range(nof_cells):
##    pbar.update(1)
##    tmp = rolls_cy(P , delays[idx,:])
##    heat_map[idx] = tmp.sum(axis=0).max()
##    #heat_map[idx] = tmp.prod(axis=0).max()
##  max_heat = heat_map.max()
##  min_heat = heat_map.min()
##  heat_map.shape = mesh_size
##  delays.shape = (*mesh_size, -1)
##  return heat_map, max_heat, min_heat


########
# Main processing here 

from multiprocessing import Pool
import os

var_dict = {}

def init_worker(P, P_shape, delays, delays_shape,mode):
  var_dict['P'] = P
  var_dict['P_shape'] = P_shape
  var_dict['delays'] = delays
  var_dict['delays_shape'] = delays_shape
  var_dict['mode'] = mode

def roll_and_sum(idx, maxi = False):
  P = np.frombuffer(var_dict['P'], dtype='float64').reshape(var_dict['P_shape'])
  delays = np.frombuffer(var_dict['delays'], dtype ='int64').reshape(var_dict['delays_shape'])
  tmp = rolls_cy(P , delays[idx,:])
  n_ech = tmp.shape[1]
  delta = 256

  if var_dict['mode'] == Mode.FAR:
    #return tmp[:,n_ech//2-delta:n_ech//2+delta].sum(axis=0).max()
    return tmp.sum(axis=0).max()
  elif var_dict['mode'] == Mode.NEAR:
    return tmp.sum(axis=0).max()




def apply_delay_mp(P, delays, nof_cells,mesh_size,mode,processes=1):
  """
  Apply delay, sum it and create heatmap (not used actually)
  arg0: P
  arg1: delays
  arg2: nof_cells
  arg3: mesh_size
  arg4: nof processes
  """
  with Pool(processes=processes, initializer=init_worker, initargs=(P, P.shape, delays, delays.shape,mode)) as pool:
    heat_map = pool.map(roll_and_sum, range(nof_cells),chunksize=nof_cells//processes*2)
  heat_map = np.array(list(heat_map))
  max_heat = heat_map.max()
  min_heat = heat_map.min()
  #heat_map[heat_map < (max_heat - (max_heat-min_heat) * 0.9)] = np.NaN
  heat_map.shape = mesh_size
  delays.shape = (*mesh_size, -1)
  return heat_map, max_heat, min_heat

  
# Just plot imshow returning axe
def plot_nearfield(USN_img, USN_ext,
             heat_map, max_heat, min_heat,
             west, south, east, north,
             idx, positions,ax,pos):
  z0 = 0
  #fig, ax = plt.subplots(figsize=(15,7))
  ax.imshow(USN_img, extent=USN_ext)
  ax.imshow(heat_map[:,:,z0].transpose(),
              extent=USN_ext,
              origin='lower',
              alpha = 0.4,
              vmin=max_heat - (max_heat-min_heat) * 0.5,
              vmax=max_heat,
              cmap=plt.get_cmap('hot'),
              )

  x,y = pos
  x = x * 10 + USN_ext[0]
  y = y * 10 + USN_ext[2]
  #ax.plot(positions[:,0], positions[:,1], 'ko')
  line = plt.Line2D( (x,x),(0,100000000), color='white',linewidth=0.5,linestyle='--')
  ax.add_artist(line)
  line = plt.Line2D( (0,100000000),(y,y), color='white',linewidth=0.5,linestyle='--')
  ax.add_artist(line)
  ax.set_aspect('equal')
  #ax.set_xlim((west, east))
  #ax.set_ylim((south, north))
  ax.set_xlabel('Lambert 93 (X)')
  ax.set_ylabel('Lambert 93 (Y)')

  return ax
  #plt.show()



