import numpy as np
import struct
import matplotlib.pyplot as plt

def hex_to_string(hex):
    if hex[:2] == '0x':
        hex = hex[2:]
    string_value = bytes.fromhex(hex).decode('utf-8')
    return string_value

def parse_udp_header(hdr):
  class Info():
    def __init__(self) :
      pass
      #self.hdr = 0
      #self.cnt = 0
      #self.gps_second = 0
      #self.gps_nano = 0
      #self.utc_second = 0
      #self.utc_nano = 0
      #self.tbb_second = 0
      #self.tbb_nano = 0
      #self.nb_ech = 0
      #self.FPGA_ver = 0
      #self.struc_ver = 0
      #self.MR_list = []
    
    def __str__(self) -> str:
      tmp_str = str(vars(self))
      tmp_str = tmp_str.replace('{', '')
      tmp_str = tmp_str.replace('}', '')
      tmp_str = tmp_str.replace(', \'', '\n\'')
      return tmp_str


  class Parameters():
    def __init__(self):
      pass
      #self.k1 = [0,0]  #[x,y]
      #self.k2 = [0,0]  #[x,y]
      #self.rising_time_min = [0,0]  #[x,y]
      #self.rising_time_max = [0,0]  #[x,y]
      #self.window_w = 0             
      #self.level_bruit_max = 0
      #self.pourcent_20 = [0,0]  #[x,y]
      #self.pourcent_80 = [0,0]  #[x,y]
      #self.polar_min = 0
      #self.polar_max = 0              
      #self.binary_test = 0
      #self.filters = [0] * 3
      #self.filters_path = [0] * 3
    def __str__(self) -> str:
      tmp_str = str(vars(self))
      tmp_str = tmp_str.replace('{', '')
      tmp_str = tmp_str.replace('}', '')
      tmp_str = tmp_str.replace(', \'', '\n\'')
      return tmp_str

    def to_fields(self,polar):
      parameters = vars(self)
      result = {}
      for key,value in parameters.items():
        if key == "filters" or key == "filters_path":
          continue
        if type(value) is list:
            result[key] = float(value[polar]) 
        else:
           result[key]= float(value)
      return result

  class Processed():
    def __init__(self):
      pass
      #self.max = [0,0]             #[x,y]
      #self.max_20 = [0,0]          #[x,y]
      #self.max_80 = [0,0]          #[x,y]
      #self.level_bruit = [0,0]     #[x,y]
      #self.rising_time = [0,0]     #[x,y]
      #self.polar_rapport = 0 

    def __str__(self) -> str:
      tmp_str = str(vars(self))
      tmp_str = tmp_str.replace('{', '')
      tmp_str = tmp_str.replace('}', '')
      tmp_str = tmp_str.replace(', \'', '\n\'')
      return tmp_str

    def to_fields(self,polar):
      parameters = vars(self)
      result = {}
      for key,value in parameters.items():
        if type(value) is list:
          result[key] = float(value[polar])
        else:
           result[key]= float(value)
      return result

  # Création de 3 objets afin de parser les différents infos de la trame
  info = Info()
  parameters = Parameters()
  processed = Processed()

  info.hdr= hdr['hdr']
  info.cnt= hdr['UDPcount']
  info.gps_second = hdr['gps_second']
  info.gps_nano = hdr['gps_nano']
  info.utc_second = hdr['utc_second']
  info.utc_nano = hdr['utc_nano']
  info.tbb_second = hdr['tbb_second']
  info.tbb_nano = hdr['tbb_nano']
  info.nb_ech = hdr['nb_ech']

  if info.hdr == b'!T4!':

    # Unpack des mots selon la structure de la trame
    word_2 = struct.unpack('>ll',hdr['word_2'])
    word_3 = struct.unpack('>hhhh',hdr['word_3'])
    word_4 = struct.unpack('>bbbbbbbb',hdr['word_4'])
    word_5 = struct.unpack('>Bbhhh',hdr['word_5'])
    word_6 = None
    word_7 = None
    word_8 = None
    word_9 = struct.unpack('>HHHH',hdr['word_9'])
    word_10 = struct.unpack('>HHHH',hdr['word_10'])
    word_11 = struct.unpack('>HHHH',hdr['word_11'])
    word_12 = None
    word_13 = None
    word_14 = struct.unpack('>HHHH',hdr['word_14'])
    
    word_15 = struct.unpack('>bbbbbbbb',hdr['word_15'])

    #exit()

    # Affectation des mots éclatés aux bons paramètres.
    info.FPGA_ver = hex(word_2[0])
    info.struc_ver = hex_to_string( str(hex(word_2[1])))
    info.MR_list = word_15

    parameters.k1 = [word_3[0],word_3[1]]    
    parameters.k2 = [word_3[2],word_3[3]]    
    parameters.rising_time_min = [word_4[0],word_4[1]]     
    parameters.rising_time_max = [word_4[2],word_4[3]]    
    parameters.pourcent_20 = [100/16*word_4[4],100/16*word_4[5]]    
    parameters.pourcent_80 = [100/16*word_4[6],100/16*word_4[7]]    
    parameters.window_w = word_5[0] 
    parameters.binary_test = word_5[1]
    parameters.level_bruit_max = word_5[2]
    parameters.polar_min = word_5[3]
    parameters.polar_max = word_5[4]
    parameters.filters = [0]*3
    parameters.filters_path = [0]*3
    if float(info.struc_ver) >= 1.01:
      for i in range(3):
        if word_14[i] >> 15 == 0: 
          filter_type = "Rejecteur"
          freq = word_14[i] & 0b0111111111111111
          if freq != 0:
            parameters.filters[i] = f"{filter_type} {freq} Mhz"
            parameters.filters_path[i]= f"{freq}_rej.conf"
        elif word_14[i] >> 15 == 1: 
          filter_type = "Band pass"
          lowfreq = (word_14[i]  & 0b0111111100000000) >> 8
          highfreq = word_14[i] & 0b0000000001111111
          parameters.filters[i] = f"{filter_type} {lowfreq}-{highfreq} Mhz"
          parameters.filters_path[i]= f"{lowfreq}_{highfreq}_band.conf"


    processed.level_bruit = [word_9[0],word_9[1]]         
    processed.max = [word_9[2],word_9[3]]                 
    processed.max_20 = [word_10[0],word_10[1]]              
    processed.max_80 = [word_10[2],word_10[3]]              
    processed.rising_time = [word_11[0],word_11[1]]          
    processed.polar_rapport = word_11[2]
  
  if info.hdr == b'!T5!':
     # Unpack des mots selon la structure de la trame
    word_2 = struct.unpack('>ll',hdr['word_2'])
    word_3 = struct.unpack('>hhhh',hdr['word_3'])
    word_4 = struct.unpack('>bbbbbbbb',hdr['word_4'])
    word_5 = struct.unpack('>Bbhhh',hdr['word_5'])
    word_6 = None
    word_7 = None
    word_8 = None
    word_9 = struct.unpack('>HHHH',hdr['word_9'])
    word_10 = struct.unpack('>HHHH',hdr['word_10'])
    word_11 = struct.unpack('>HHHH',hdr['word_11'])
    word_12 = None
    word_13 = None
    word_14 = struct.unpack('>HHHH',hdr['word_14'])
    
    word_15 = struct.unpack('>bbbbbbbb',hdr['word_15'])

    #exit()

    # Affectation des mots éclatés aux bons paramètres.
    info.FPGA_ver = hex(word_2[0])
    info.struc_ver = hex_to_string( str(hex(word_2[1])))

    parameters.k1 = [word_3[0],word_3[1]]    
    parameters.filters = [0]*3
    parameters.filters_path = [0]*3
    if float(info.struc_ver) >= 1.01:
      for i in range(3):
        if word_14[i] >> 15 == 0: 
          filter_type = "Rejecteur"
          freq = word_14[i] & 0b0111111111111111
          if freq != 0:
            parameters.filters[i] = f"{filter_type} {freq} Mhz"
            parameters.filters_path[i]= f"{freq}_rej.conf"
        elif word_14[i] >> 15 == 1: 
          filter_type = "Band pass"
          lowfreq = (word_14[i]  & 0b0111111100000000) >> 8
          highfreq = word_14[i] & 0b0000000001111111
          parameters.filters[i] = f"{filter_type} {lowfreq}-{highfreq} Mhz"
          parameters.filters_path[i]= f"{lowfreq}_{highfreq}_band.conf"



  return info,parameters,processed


def TBB_read_dat(*args):
  """
  Function copied from tbb_eparse library
  Allow to read .dat files from tbb trig eparse 
  """
  fichier = args[0]
  #print ('Fichier: ', fichier)
  Nb_pol = 2
  Nb_MR = 4

  dt_file = np.dtype([
            ('hdr',		'|S4'	),
            ('UDPcount',   np.uint32),
            ('gps_second', np.uint32),
            ('gps_nano',   np.uint32),
          ])
  dat = np.frombuffer(fichier, dtype=dt_file, count=1)[0]

  if dat['hdr'] == b'!T4!' or dat['hdr'] == b'!T5!':
    ### T4 new format #####
    dt_file = np.dtype([
              ('hdr',		'|S4'	),
              ('UDPcount',   np.uint32),
              ('gps_second', np.uint32),
              ('gps_nano',   np.uint32),
              ('word_2', np.uint64),
              ('word_3', np.uint64),
              ('word_4', np.uint64),
              ('word_5', np.uint64),
              ('word_6', np.uint64),
              ('word_7', np.uint64),
              ('word_8', np.uint64),
              ('word_9', np.uint64),
              ('word_10', np.uint64),
              ('word_11', np.uint64),
              ('word_12', np.uint64),
              ('word_13', np.uint64),
              ('word_14', np.uint64),
              ('word_15', np.uint64),
              ('utc_second', np.uint32),
              ('utc_nano',   np.uint32),
              ('tbb_second', np.uint32),
              ('tbb_nano',   np.uint32),
              ('nb_ech',	 np.uint32),
              ])
    dat = np.frombuffer(fichier, dtype=dt_file, count=1)[0]
    Nb_samples = dat['nb_ech']//(Nb_MR * Nb_pol)
    dt_file = np.dtype([
            ('hdr',		'|S4'	),
            ('UDPcount',   np.uint32),
            ('gps_second', np.uint32),
            ('gps_nano',   np.uint32),
            ('word_2', np.uint64),
            ('word_3', np.uint64),
            ('word_4', np.uint64),
            ('word_5', np.uint64),
            ('word_6', np.uint64),
            ('word_7', np.uint64),
            ('word_8', np.uint64),
            ('word_9', np.uint64),
            ('word_10', np.uint64),
            ('word_11', np.uint64),
            ('word_12', np.uint64),
            ('word_13', np.uint64),
            ('word_14', np.uint64),
            ('word_15', np.uint64),
            ('utc_second', np.uint32),
            ('utc_nano',   np.uint32),
            ('tbb_second', np.uint32),
            ('tbb_nano',   np.uint32),
            ('nb_ech',	 np.uint32),
            
            ('data',	   np.int16, (Nb_samples, Nb_MR, Nb_pol)),
            ])
    dat = np.frombuffer(fichier, dtype=dt_file, count=1)[0]
    
  elif dat['hdr'] == b'!T3!':
    ### T3 old format #####
    dt_file = np.dtype([
              ('hdr',		'|S4'	),
              ('UDPcount',   np.uint32),
              ('gps_second', np.uint32),
              ('gps_nano',   np.uint32),
              ('utc_second', np.uint32),
              ('utc_nano',   np.uint32),
              ('tbb_second', np.uint32),
              ('tbb_nano',   np.uint32),
              ('nb_ech',	 np.uint32),
              ])
    dat = np.frombuffer(fichier, dtype=dt_file, count=1)[0]
    Nb_samples = dat['nb_ech']//(Nb_MR * Nb_pol)
    dt_file = np.dtype([
              ('hdr',		'|S4'	),
              ('UDPcount',   np.uint32),
              ('gps_second', np.uint32),
              ('gps_nano',   np.uint32),
              ('utc_second', np.uint32),
              ('utc_nano',   np.uint32),
              ('tbb_second', np.uint32),
              ('tbb_nano',   np.uint32),
              ('nb_ech',	 np.uint32),

              ('data',	   np.int16, (Nb_samples, Nb_MR, Nb_pol)),
              ])
    dat = np.frombuffer(fichier, dtype=dt_file, count=1)[0]
  else:
    print('Error: unknown frame type')
    exit()

  info,parameters,processed = parse_udp_header(dat)


  return dat, info, parameters,processed