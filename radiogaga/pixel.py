# %%
import numpy as np

import matplotlib.pyplot as plt
from scipy import signal
import glob
from importlib import reload
from scipy.fftpack import rfft, fftshift
import scipy.signal
from radiogaga import TBBurst_lib as tbb
from multiprocessing import Pool
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.ticker import AutoMinorLocator, FormatStrFormatter,MaxNLocator
import argparse
import os
import yaml
from tqdm import tqdm


board_cfg = tbb.load_yaml()
nof_chan = 4
nof_polar = 2
# Define shape of image
#point_par_pixel = 16000 

class Pixel():

  def __init__(self,path,point_par_pixel):
    self.path = path
    self.point_par_pixel = point_par_pixel
    self.nof_points = int(1e9)
    self.nof_blocs =  self.nof_points // self.point_par_pixel
    self.H = int(np.sqrt(self.nof_points/self.point_par_pixel))

    self.ratio = 1
    self.h_pixel = int(self.H*1/self.ratio) 
    self.v_pixel = int(self.H*self.ratio)
    self.scalex = self.v_pixel * self.point_par_pixel * 5e-9 * 1000
    self.scaley = self.h_pixel * self.scalex / 1000
    print( self.h_pixel*  self.v_pixel)

    self.files, self.mr_list = tbb.load_dump(self.path)
    self.stats, self.process = self.init_output_var(self.path,len(self.files))
    

    if self.process:
      self.stats = self.run_process(self.files,self.stats)
    self.stats = self.stats[:self.h_pixel *  self.v_pixel,:].reshape(self.h_pixel,self.v_pixel,len(self.files),nof_chan,nof_polar)
    print(self.stats.shape)
  
  def plot_pdf(self):
    self.__plot_pdf(self.stats,self.mr_list,self.path)


  def init_output_var(self,path,nof_files):
    dt_stats = np.dtype([('PAYLOAD' , 'float32')])
    # Check if an output file already exist
    out_exist = glob.glob(path + f"/output/bin/pixel{self.point_par_pixel}.bin")
   

    if(len(out_exist) == 0):
      print("No output files found, running process....")
      stats = np.memmap(f'{path}/output/bin/pixel{self.point_par_pixel}.bin', dtype=dt_stats, mode='w+',shape = ((self.nof_blocs,nof_files,nof_chan,nof_polar)))
      process = True
    else:
      answer = input("An output file as been found, I open it and don't run the process, do you want to load it directly ? y/n\n")
      if answer.lower() == 'y':
        stats = np.memmap(f'{path}/output/bin/pixel{self.point_par_pixel}.bin', dtype=dt_stats, mode='r',shape = ((self.nof_blocs,nof_files,nof_chan,nof_polar)))
        process = False
      else: 
        print("No output files found, running process....")
        stats = np.memmap(f'{path}/output/bin/pixel{self.point_par_pixel}.bin', dtype=dt_stats, mode='w+',shape = ((self.nof_blocs,nof_files,nof_chan,nof_polar)))
        process = True
    return stats,process
  
  def plot_axe(self,board,chan,polar,ax0):
    
    
    im0 = ax0.imshow(self.stats['PAYLOAD'][:,:,board,chan,polar],interpolation="none",origin='lower',extent=[0,self.scalex,0,self.scaley], vmin = 25 , vmax = 150, aspect=(self.scalex*1/self.ratio)/(self.scaley*self.ratio))#aspect=(scalex*2)/(scaley*0.5)) # aspect = "equal" ne veut pas marcher""""""
    ax0.set_ylabel('Time seconde (s)')
    ax0.set_xlabel('Time (ms)')
    #ax0.
    if polar == 0:
      ax0.set_title("Polar NE")
    elif polar == 1:
      ax0.set_title("Polar NO")

    ax0.yaxis.set_minor_locator(AutoMinorLocator(10))
    ax0.yaxis.set_major_locator(MaxNLocator(25)) 

    return im0
      
 
  def __plot_pdf(self,stats,files_name,path):
    print(stats.shape)
    nof_files = len(files_name)
    stats = stats.reshape(self.H,self.H,nof_files,nof_chan,nof_polar)
    plt.rcParams.update({'figure.max_open_warning': 0})
    print("Start ploting ...")
    with tqdm(total = nof_files*4) as pbar:
      with PdfPages(f'{path}/output/pdf/pixel_{self.point_par_pixel}.pdf') as pdf:
        for board in range(nof_files):
          for chan in range(4):
            fig, (ax0,ax1) = plt.subplots(ncols=2,figsize=(15,7))
            MR = files_name[board]['MR'][chan]
            rot = files_name[board]['rotation'][chan]
            fig.suptitle("")
            fig.suptitle(f"MR{MR}   rotation : {rot}°", fontsize=16) # Title 

            im0 = self.plot_axe(board,chan,0,ax0)
            im1 = self.plot_axe(board,chan,1,ax1)
            #ax0.set_rasterized(True)
            #ax1.set_rasterized(True)
            fig.colorbar(im0, ax=ax0)
            fig.colorbar(im0, ax=ax1)

            pdf.savefig(fig,bbox_inches='tight',dpi=1000)

            #plt.savefig(args.path+"pixel.png")
            pbar.update(1)
    print("End ploting ...")

  def run_process(self,files,stats,processes = 1):
    """
    Multiprocess gestion 
    """
    nof_files = len(files)
    my_args = []
    for board in range(len(files)):
      for chan in range(4):
        for polar in range(2):
          my_args.append((files[board][:,chan*2+polar],self.point_par_pixel,self.nof_points))

    print("Start processing ...")
    with Pool(processes=processes) as pool:
    # r = list(tqdm.tqdm(p.imap(_foo, range(30)), total=30))
      tmp = list(tqdm(pool.imap(process,my_args),total=nof_files*nof_chan*nof_polar))
    tmp = np.array(tmp)

    tmp = np.transpose(tmp)
    tmp = np.reshape(tmp,(-1,nof_files,4,2))
    stats['PAYLOAD'] = tmp
    print("End processing ...")
    return stats

  def trig_hot_points(self,seuil,MR=None,polar=None):
    #board = 0
    #chan = 3
    #polar = 0
    #stat = stats['PAYLOAD'][:,board,chan,polar]
    nof_hot_points_max = 0
    hot_points_ouput = []
    shift_ouput = []

    if MR != None and polar!=None:
      board,chan = tbb.get_board_chan_from_mr(self.mr_list,MR)

      stat = self.stats['PAYLOAD'][:,:,board,chan,polar]
      stat = stat.reshape(-1)
      file = self.files[board][:,chan*2+polar]
      file = file[:self.nof_points]
      file = file.reshape(-1,self.point_par_pixel)
      # Return position of hot points
      hot_points = np.where(stat>seuil)[0]
      # Calcul shift
      maxi = np.max(file[hot_points,:],axis=1)
      shift = np.empty((len(maxi)),dtype='int16')

      for i in range(len(hot_points)):
            shift[i] = np.where(file[hot_points[i]] == maxi[i])[0][0]

      shift = self.point_par_pixel // 2 - shift

      return hot_points,shift, MR

    else:
      for board in range(len(self.files)):
        for chan in range(4):
          for polar in range(2):
            print(board,chan,polar)
            print(self.stats.shape)
            stat = self.stats['PAYLOAD'][:,:,board,chan,polar]
            stat = stat.reshape(-1)
            print(stat.shape)
            file = self.files[board][:,chan*2+polar]
            file = file[:self.nof_points]
            file = file.reshape(-1,self.point_par_pixel)
            hot_points = np.where(stat>seuil)[0]
            maxi = np.max(file[hot_points,:],axis=1)
            shift = np.empty((len(maxi)),dtype='int16')
          
            for i in range(len(hot_points)):
              shift[i] = np.where(file[hot_points[i]] == maxi[i])[0][0]

            shift = self.point_par_pixel // 2 - shift

            if len(hot_points) > nof_hot_points_max:
              nof_hot_points_max = len(hot_points)
              hot_points_ouput = hot_points
              shift_ouput = shift
              MR = ( board,chan,polar)
            #output = np.dstack((hot_points,shift))
            #output = output.reshape(len(hot_points),2)
      return hot_points_ouput,shift_ouput, MR # Block, pulse shift to apply in the block , MR(board,chan,polar)



# Main function
def process(argv):
  #board=argv[0][0] # idx for board
  #chan= argv[0][1]  # idx for channel
  #polar=argv[0][2] # idx for polar 
  print(1)
  file = argv[0]
  print(2)
  point_par_pixel = argv[1]
  nof_points = argv[2]
  #stats = argv[2]
  file = file[:nof_points]
  print(3)
  file = file.reshape(-1,point_par_pixel)
  print(4)
  median = np.median(file, axis=1)
  print(5)
  debiased = file - median[:,np.newaxis]
  print(6)
  rected = np.abs(debiased)
  print(7)
  #mad = np.median(rected, axis=1)
  abs_max = rected.max(axis=1)
  print(8)
  #stats[:,board,chan,polar] = abs_max
  return abs_max




def export_hot_points_txt(files,hot_points,shift,ma_list,yaml,path): 
  chunk_len = 16000
  list_hot_points = ""
  n_ech = files[0].shape[0]
  n_MR = len(ma_list)
  data = np.zeros(shape=(1000,n_MR*2))
  
  #file = file[:nof_points]
  #file = file.reshape(-1,point_par_pixel,8)
  
  for hot_idx ,hot_point in enumerate(hot_points):
    n0 = hot_points[hot_idx] * chunk_len - shift[hot_idx]
    n_point = 1000
    n0 += chunk_len//2 - n_point//2
    for idx,mymr in enumerate(ma_list):
      board,chan = tbb.get_board_chan_from_mr(yaml,mymr)
      data[:,idx*2] = files[board][n0:n0+n_point,chan*2]
      data[:,idx*2+1] = files[board][n0:n0+n_point,chan*2+1]  
    
    
    basename = f"{str(hot_point).zfill(10)}.csv"
    list_hot_points += basename + "\n"

    np.savetxt(f"{path}/output/txt/{basename}",data,delimiter=" ",fmt="%d")
    
  with open(f"{path}/output/txt/list.txt",'w') as f:
    f.write(list_hot_points)
