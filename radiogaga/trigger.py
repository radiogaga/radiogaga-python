import numpy as np
from scipy import signal
import matplotlib.pyplot as plt

import context
from radiogaga import TBBurst_lib as tbb
from radiogaga import pointing

class Trigger():
  def __init__(self,files,lmn,ma_list,positions,filters_path,event_pos,oversample = 1):
    self.oversample = oversample
    self.filters_path = filters_path
    self.process(files,lmn,ma_list,positions,event_pos)
    


  def process(self,files,lmn,ma_list,positions,event_pos):
    self.ma_list = ma_list
    self.n_ech = files[0].shape[0] 
    self.nof_MR = len(ma_list)
    delta_filter = 200 # Delay ajouté par les filtres numériques
    self.data = np.zeros(shape=(self.n_ech+delta_filter,self.nof_MR,2))
    yaml = tbb.load_yaml()
    window_tukey = signal.tukey(self.n_ech,0.2, sym=True)
    window_tukey = np.concatenate((window_tukey,np.zeros(shape=(delta_filter))))
    if self.filters_path[0] !=  0 and self.filters_path[1] !=  0 and self.filters_path[2] !=  0 and False:
      filt_1 = np.loadtxt(f'{tbb.ROOT_DIR}/config/{self.filters_path[0]}')
      filt_2 = np.loadtxt(f'{tbb.ROOT_DIR}/config/{self.filters_path[1]}')
      filt_3 = np.loadtxt(f'{tbb.ROOT_DIR}/config/{self.filters_path[2]}')
    else:
      print("Filter are not detected in the frame, applying default filters")
      filt_1 = np.loadtxt(f'{tbb.ROOT_DIR}/config/41_76_band.conf')
      filt_2 = np.loadtxt(f'{tbb.ROOT_DIR}/config/36_rej.conf')
      filt_3 = np.loadtxt(f'{tbb.ROOT_DIR}/config/75_rej.conf')
      

    # Convert to np array
    for idx,mymr in enumerate(ma_list):
      board,chan = tbb.get_board_chan_from_mr(yaml,mymr)
      #print(board,chan)
      self.data[0:self.n_ech,idx,0] = files[board][:,chan*2]
      self.data[0:self.n_ech,idx,1] = files[board][:,chan*2+1]
    
      
    ### Etalon for test
    #self.data = np.ones(shape=(self.n_ech+delta_filter,self.nof_MR,2))
    #self.data = np.random.normal(0,15,size=self.data.shape)
    #self.data[event_pos-delta_filter-50,:,:] = 300
    #############
    ### Pointage event etalon
    #############
    #self.data_phased = np.zeros(shape=(self.n_ech,self.nof_MR,2))
    #lmn_event = pointing.alt_az_to_lmn(65,136)
    #for idx, mr_pos in enumerate(positions):
    #  delay = pointing.get_delay_from_lmn(mr_pos,lmn_event)
    #  self.data[:,idx,0]  = np.roll(self.data[:,idx,0],delay)
    #  self.data[:,idx,1]  = np.roll(self.data[:,idx,1],delay)
    #  #print(delay,end=" ")

    

    # Apply a tukey window
    self.data = self.data[:] * window_tukey[:,np.newaxis,np.newaxis]
    #self.data_phased = self.data_phased[:] * window_tukey[:,np.newaxis,np.newaxis]

    ###########
    # Filter 
    ###########
    self.data_filtred = np.zeros(shape=(self.n_ech+delta_filter,self.nof_MR,2))
    # Apply filters

    self.data_filtred = signal.lfilter(filt_3, 1, self.data, axis=0) // 2**15
    self.data_filtred = signal.lfilter(filt_2, 1,self.data_filtred, axis=0) // 2**15
    self.data_filtred = signal.lfilter(filt_1, 1,self.data_filtred, axis=0) // 2**15 
    self.data_filtred = self.data_filtred[0+delta_filter:self.n_ech+delta_filter]
    
    if self.oversample > 1:
      self.n_ech = self.n_ech * self.oversample
      self.delta_filter = self.data_filtred * self.oversample
      self.data_filtred = signal.resample(self.data_filtred,self.data_filtred.shape[0]*self.oversample)
           
    
    ############
    ## Pointage 
    ############
    self.data_phased_filtred = np.zeros(shape=(self.n_ech,self.nof_MR,2))
    for idx, mr_pos in enumerate(positions):
      delay = pointing.get_delay_from_lmn(mr_pos,lmn,te=5e-9/self.oversample) - 256
      self.data_phased_filtred[:,idx,0]  = np.roll(self.data_filtred[:,idx,0],delay)
      self.data_phased_filtred[:,idx,1]  = np.roll(self.data_filtred[:,idx,1],delay)
      #print(delay,end=" ")


    #self.data_phased_filtred = np.zeros(shape=(self.n_ech+delta_filter,self.nof_MR,2))
    #self.data_phased_filtred = signal.lfilter(filt_1, 1, self.data_phased, axis=0) // 2**15
    #self.data_phased_filtred = signal.lfilter(filt_2, 1,self.data_phased_filtred, axis=0) // 2**15
    #self.data_phased_filtred = signal.lfilter(filt_3, 1,self.data_phased_filtred, axis=0) // 2**15 
    #self.data_phased_filtred = self.data_phased_filtred[0+delta_filter:self.n_ech+delta_filter]

    


    #######
    # Sum
    #######
    #np.reshape(self.data_phased_filtred)
    #self.data_phased_filtred.astype(float)
    median = np.median(self.data_phased_filtred,axis=0)
    #mad = np.median(np.abs(self.data_phased_filtred-median),axis=(0))[np.newaxis,:,:] #*1.48 # C'est Louis qui l'a dit
    mad = np.std(self.data_phased_filtred[:,:,:],axis=0)[np.newaxis,:,:]
    max = np.max(self.data_filtred,axis=0)
    self.data_filtred_std_mad = self.data_filtred / mad
    self.data_filtred_std_max = self.data_filtred / max * 100
    
    self.data_phased_filtred_std = self.data_phased_filtred / mad
    self.data_phased_filtred_sommed_std = (self.data_phased_filtred_std).sum(axis=1)
    self.data_phased_filtred_sommed = (self.data_phased_filtred).sum(axis=1)



    #self.data_phased_sommed = (self.data_phased).sum(axis=1)

    # Real output filter like FPGA
    #self.data_phased_sommed_filtred = signal.lfilter(filt_1, 1, self.data_phased_sommed, axis=0) // 2**15
    #self.data_phased_sommed_filtred = signal.lfilter(filt_2, 1,self.data_phased_sommed_filtred, axis=0) // 2**15
    #self.data_phased_sommed_filtred = signal.lfilter(filt_3, 1,self.data_phased_sommed_filtred, axis=0) // 2**15
    #self.data_phased_sommed_filtred = self.data_phased_sommed_filtred[0+delta_filter:self.n_ech+delta_filter,:]


    # Puissance cumulée glissante
    ech_convolve = self.data_phased_filtred_sommed.shape[0] + 128* self.oversample -1
    P = np.zeros(shape=(ech_convolve,2))
    n = self.data_filtred.shape[0]
    n=5
    ech_convolve = self.data_filtred.shape[0] + n * self.oversample -1
    self.P_data_convolve= np.zeros(shape=(ech_convolve,self.nof_MR,2))
    self.P_data_phased= np.zeros(shape=(ech_convolve,self.nof_MR,2))
    ech_convolve = self.data_filtred.shape[0] + 128 * self.oversample -1
    self.P_data_convolve_128= np.zeros(shape=(ech_convolve,self.nof_MR,2))

    self.P_data= np.zeros(shape=(self.n_ech,self.nof_MR,2))
    

    for polar in range(2):
      P[:,polar]=(np.convolve(self.data_phased_filtred_sommed[:,polar] ** 2, np.ones(128*self.oversample)/128*self.oversample, mode='full'))
      for idx in range(self.nof_MR):
        self.P_data_convolve[:,idx,polar] = np.convolve(self.data_filtred[:,idx,polar] ** 2, np.ones(n*self.oversample)/n*self.oversample, mode='full')
        self.P_data_convolve_128[:,idx,polar] = np.convolve(self.data_filtred[:,idx,polar] ** 2, np.ones(128*self.oversample)/128*self.oversample, mode='full')
    
        self.P_data[:,idx,polar] = self.data_filtred[:,idx,polar] ** 2
        self.P_data_phased[:,idx,polar] = np.convolve(self.data_phased_filtred[:,idx,polar] ** 2, np.ones(n*self.oversample)/n*self.oversample, mode='full')
    self.P = P[:self.n_ech,:]
    #self.P_data = P_data[:self.n_ech,:,:].copy(order='F')
    #self.P_data_convolve = P_data_convolve[:self.n_ech,:,:].copy(order='F')
    #self.P_data_phased = P_data_phased[:self.n_ech,:,:].copy(order='F')
    # Dérivé 
    self.derive = (P - np.roll(P,7*self.oversample,axis=0)) // 8*self.oversample
    self.derive_data = (self.P_data_convolve_128 - np.roll(self.P_data_convolve_128,8,axis=0)) 
    self.derive_data_phased = (self.P_data_phased - np.roll(self.P_data_phased,1,axis=0)) 
    
    # Get position of events
    # Trig on the derivated of the accumulated power
    # Set 100 value on sample above it
    seuil = 125
    index = np.where(self.derive_data >= seuil)
    position = np.zeros((self.n_ech,self.data_filtred.shape[1],self.data_filtred.shape[2]))
    position[index] = 100 
    # Then take the first sample
    position2 = np.zeros((self.n_ech,self.data_filtred.shape[1],self.data_filtred.shape[2]))
    index = np.argmax(position==100,axis=0)
    # Then set to 100 on N sample around the index
    n = 1
    for mr in range(index.shape[0]):
      position2[index[mr,0]-n:index[mr,0]+n,mr,0] = 100
      position2[index[mr,1]-n:index[mr,1]+n,mr,1] = 100
    # Or just 1 sample, but not so much precision
    #np.put_along_axis(position2,index[np.newaxis,:,:],100,axis=0)
    position2[0,:,:] = 0 # Fix for no trigged MR
    self.data_position = position2
    self.data_position_phased = np.zeros((self.n_ech,self.data_filtred.shape[1],self.data_filtred.shape[2]))
    for idx, mr_pos in enumerate(positions):
      delay = pointing.get_delay_from_lmn(mr_pos,lmn,te=5e-9/self.oversample) - 256
      self.data_position_phased[:,idx,0]  = np.roll(self.data_position[:,idx,0],delay)
      self.data_position_phased[:,idx,1]  = np.roll(self.data_position[:,idx,1],delay)



    
    #S/N
    datamax = (self.data_phased_filtred_sommed[(event_pos*self.oversample)-100*self.oversample:(event_pos*self.oversample)+100*self.oversample])**2
    datamean = (self.data_phased_filtred_sommed[0+50*self.oversample:event_pos*self.oversample-50*self.oversample])**2
    maxi = datamax.max(axis=0)
    mean = datamean.mean(axis=0)
    #plt.figure()
    #plt.plot(datamax)
    #plt.plot(datamean)
    #plt.show()

    self.SN = np.sqrt(maxi) / np.sqrt(mean)

  def np_to_file(self,data): 
    files_phased = []
    for idx in range(len(self.ma_list)//4):
        files_phased.append(np.resize(self.data[:,idx*4:idx*4+4,:],(self.n_ech,8)))
    return files_phased