from datetime import date, datetime
import numpy as np

class Altaz():
  def __init__(self,file):
    if file[-1] == 'B': #if altazB
      dtype={'names': ('time','ana','beam','az','alt', 'l', 'm','n'),
              'formats': ('datetime64[s]','i4', 'i4', 'f4', 'f4', 'f4', 'f4', 'f4')}
      self.altaz = np.loadtxt(file, dtype=dtype, comments=';',usecols=(0,1,2,3,4,5,6,7))
      print(type(self.altaz))
      print(len(self.altaz.shape))
    if file[-1] == 'A': #if altazA
      dtype={'names': ('time','az','alt'),
              'formats': ('datetime64[s]', 'f4', 'f4')}
      self.altaz = np.loadtxt(file, dtype=dtype, comments=';',usecols=(0,1,2))


  def get_pointing_from_datetime(self,time,beam=None):
    if len(self.altaz.shape) == 0:
      alt = self.altaz['alt']
      az = self.altaz['az']
      lmn = (self.altaz['l'], self.altaz['m'], self.altaz['n'])
      return alt,az,lmn
    else:
      try: 
        time = time.replace(tzinfo=None) # Important pour supprimer le temps UTC  https://stackoverflow.com/questions/796008/cant-subtract-offset-naive-and-offset-aware-datetimes
        if beam is None: # Si aucun beam est passé on prend le dernier, c'est fait comme ça dans le software
          beam = self.altaz['beam'].max()
        occurence = np.where((self.altaz['time'] >= time) & (self.altaz['beam'] == beam))[0][0]
        alt = self.altaz['alt'][occurence-1]
        az = self.altaz['az'][occurence-1]
        lmn = (self.altaz['l'][occurence-1], self.altaz['m'][occurence-1], self.altaz['n'][occurence-1])
        return alt,az,lmn
      except:
        #assert False, f"No pointing for datetime{time} and beam {beam}"
        return 90,0,(0,0,1)

  def check_if_ana_pointing(self,time):

    time = time.replace(tzinfo=None) # Important pour supprimer le temps UTC  https://stackoverflow.com/questions/796008/cant-subtract-offset-naive-and-offset-aware-datetimes
    time = time.replace(microsecond=0) # Remove microse pour avoir la même precisont que le altaz
    occurence = np.where((self.altaz['time'] == time))[0]
    if len(occurence) > 0: return True
    else : return False
