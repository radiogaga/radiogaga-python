from datetime import datetime

class Parset():
  def __init__(self,file):
    with open(file,'r') as NenuFAR_parset_fid:
      for line in NenuFAR_parset_fid.readlines():
          if line.startswith('AnaBeam[0].startTime'):
              # NICKEL V0 makes use of AnaBeam[0].startTime to start correlation
              # This will probably change
              self.BeamStartTime = self.__get_datetime__(line)
              self.ObsID = self.BeamStartTime.strftime("%y%j%H%M")  #  Must fit into an uint32
              continue
          if line.startswith('Observation.startTime'):
              self.ObsStartTime = self.__get_datetime__(line)
              continue
          if line.startswith('Observation.stopTime'):
              self.ObsStopTime = self.__get_datetime__(line)
              continue
          if line.startswith('Observation.name'):
              self.ObsName = line.split('=')[1].strip().replace('"','')
              continue
          if line.startswith('Observation.topic'):
              self.ObsTopic = line.split('=')[1].strip()
              print(self.ObsTopic)
              self.KP = self.ObsTopic.split(" ")[0]
              print(self.KP)
              continue

  def __get_datetime__(self,line):
    NenuFARStrTimeFmt = "%Y-%m-%dT%H:%M:%SZ"
    return datetime.strptime(line.split('=')[1].strip(), NenuFARStrTimeFmt)