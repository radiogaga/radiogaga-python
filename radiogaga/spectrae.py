import numpy as np
from scipy.fft import fft,rfft,fftfreq
import scipy



#class fft:
#  def __init__(self) -> None:
#    pass
#
#  def calcul(self,data):
#    n_point = data.shape[0] 
#    #data = data * W[np.newaxis, :, np.newaxis]
#    self.f = np.arange(n_point // 2 + 1) / n_point * Fe
#    W = scipy.signal.windows.blackmanharris(n_point,sym=False)
#    if len(data.shape) == 3: # data not sommed
#      data = data * W[:,np.newaxis,np.newaxis]
#      fftx = np.fft.rfft(data[:,:,0],axis=0)
#      ffty = np.fft.rfft(data[:,:,1],axis=0)
#      fftx = abs2(fftx)
#      ffty = abs2(ffty)
#    elif len(data.shape) == 2: # data  sommed
#      data = data * W[:,np.newaxis]
#      fftx = np.fft.rfft(data[:,0],axis=0)
#      ffty = np.fft.rfft(data[:,1],axis=0)
#      fftx = abs2(fftx)
#      ffty = abs2(ffty)
#    
#    return fftx,ffty

def abs2(x):
  return x.real**2 + x.imag**2

class Spectrae:
  def __init__(self,data,Te=1e-9) -> None:
    self.Te = Te
    self.data = data.copy()
    self.calcul()

  def calcul(self):
    print(self.data.shape)
    self.data = np.reshape(self.data,(-1,32,2,),order='F')
    print(self.data.shape)
    self.n_ech = self.data.shape[0]
    W = scipy.signal.windows.blackmanharris(self.n_ech,)
    #self.data = self.data * W[:,np.newaxis,np.newaxis]
    self.dataf =abs2(rfft(self.data,axis=0,))
    self.dataf = 10 * np.log10(self.dataf[0:self.n_ech//2])
    self.xf = fftfreq(self.n_ech,self.Te)[:self.n_ech//2]

  def plot(self,ax,spectre=None):
    if len(self.data.shape) == 2:
      ax.plot(self.xf,2.0/self.n_ech * self.dataf)
    if len(self.data.shape) == 3:
      assert spectre != None, "The spectrae n° should be passed in mode spectrogram"
      ax.plot(self.xf,self.dataf[:,spectre,:])

  def plot_spectrogram(self,ax,polar):
    ax.imshow(self.dataf[:,:,polar].transpose(),
              aspect='auto',
              vmin=50,
              vmax=80)
            