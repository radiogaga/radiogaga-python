from re import M
import numpy as np
import glob 
import matplotlib.pyplot as plt
import math

import context

from radiogaga import near_field as nf

# CONSTANT
LOFAR_center = (639133.971 ,6697600.364 ,182.096)
# pattern, 

pattern = {
  # All mr
  "allmr" : (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79),
  "oldmr" : (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56),
  #All mr with distant
  "allmr_dist" : (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 101, 102, 103),
  # actual pattern
  "actual_pattern" : (64, 66, 68, 70, 72, 75, 78, 79),
  "periph" : sorted((18,20,21,27,28,50,38,39,40,66,70,77,79,62,60,58,57,55,53,54,101,102,103)),
  "4mr" : (28,39,55,77,101),
  # Round shape pattern
  "round_pattern" : (34, 33, 32, 41, 42, 45, 37, 35),
  # new round shape pattern
  "new_round_pattern" : (33, 35, 37, 41, 42, 43, 44, 45),
  "new_round_pattern_ext" : (12,24,33, 35, 37,38, 41, 42, 43, 44, 45,64),
  # Line NS pattern
  "ns_pattern" : (27, 26, 25, 11, 10, 6, 3),
  # Line NE pattern
  "ne_pattern" : (74, 78, 61, 59),
  # Line NW pattern
  "nw_pattern" : (73, 72, 67, 64),
  # Cross NE/NW pattern
  "cross_pattern" : (74, 78, 61, 59, 73, 72, 67, 64)
}


def loag_altazB(path):
    altazb = glob.glob(f"{path}*.altazB")[0]
    print(f"Loading {altazb}")
    lmn = np.loadtxt(altazb,comments=';',usecols=(5,6,7))
    return lmn

def get_lmn_from_datetime(path,date,time):
  """
  Return a tuple of lmn on the wanted date
  args 1: Path where locating the altazB file
  args 2: date of the poiting loaded ( format : YYYYMMDD)
  args 3: time of the poiting loaded ( format : HHMMSS) 
  """
  print(date,time)
  altazb = glob.glob(f"{path}/*.altazB")
  if len(altazb) > 0:
    dtype={'names': ('time', 'l', 'm','n'),
            'formats': ('datetime64[s]', 'f4', 'f4', 'f4')}
    lmn = np.loadtxt(altazb[0], dtype=dtype, comments=';',usecols=(0,5,6,7))


    datetime = date[0:4] + "-" + date[4:6] + "-" + date[6:8]  + "T" + time[0:2] + ":" + time[2:4] + ":" + time[4:6]
    datetime = np.datetime64(datetime)
    
    
    try:
      print(np.where(lmn['time'] >= datetime)[0][0])
    
      occurence = np.where(lmn['time'] >= datetime)[0][0]
      print('occurence',occurence)
      print(lmn[occurence-1])
      lmn = tuple(lmn[occurence-1])
      
    except:
      print(f'No pointing found for {datetime}')
      lmn = (0,0,0,0)
  else:
    lmn = (0,0,0,0)
  return lmn[1:4]
  

def get_delay_from_lmn(position,lmn,te=5e-9):
  """
  Return a delay in echantillon (200Mhz) for a MR
  arg1 : MR position
  arg2 : tuple of lmn (from get_lmn_from_datetime function)
  """
  if lmn != (0,0,0):
    if position.shape[0] == 3:
      MRposition = np.subtract(position,LOFAR_center)
      scal = lmn[0] * MRposition[0] + lmn[1] * MRposition[1] +lmn[2] * MRposition[2]
      delayns = scal / 299792458.0

      delayech = round(delayns / te + 256) # On ajoute 256 pour avoir le même comportement que dans le FPGA, cet offset sert à ne pas avoir de délai négatif
    else:
      MRposition = np.subtract(position,LOFAR_center)
      scal = lmn[0] * MRposition[:,0] + lmn[1] * MRposition[:,1] +lmn[2] * MRposition[:,2]
      delayns = scal / 299792458.0
      delayech = np.round(delayns / te + 256) # On ajoute 256 pour avoir le même comportement que dans le FPGA, cet offset sert à ne pas avoir de délai négatif


  else:
    delayech = 256
  return delayech

def plot_3d_view(lmn_pointing,lmn_events=None,mr_position=None):
  """
  Plot a 3D view of pointing source and/or events source and/or mr positions of

  arg1: tuple or np array of lmn source 
  arg2: tuple or np array of lmn events
  arg3: np array of mr positions tranlated between 0 and 1 (example : (positions - (pointing.LOFAR_center)) /400 )
  """
  fig = plt.figure()
  ax = plt.axes(projection='3d')
  print(type(lmn_pointing))
  # PLot source
  if type(lmn_pointing) == tuple:
    print(1)
    ax.quiver(0,0,0,lmn_pointing[0],lmn_pointing[1],lmn_pointing[2],color='blue',label="Pointing")
  elif type(lmn_pointing) == np.ndarray:
    print(2)
    ax.scatter(lmn_pointing[:,0],lmn_pointing[:,1],lmn_pointing[:,2],color='blue',label="Pointing")

  # PLot events
  if type(lmn_events) != None:
    if type(lmn_events) == tuple:
      print(3)
      ax.scatter(lmn_events[0],lmn_events[1],lmn_events[2],color='red',label="Events")
    elif type(lmn_events) == np.ndarray:
      print(4)
      ax.scatter(lmn_events[...,0],lmn_events[...,1],lmn_events[...,2],color='red',label="Events")

  # plot MR position 
  if type(mr_position) != type(None):
    ax.scatter(mr_position[:,0],mr_position[:,1],0,color='black',label="MR")

  # Legend, limit etc..
  ax.text(1,0,0,'E')
  ax.text(-1,0,0,'W')
  ax.text(0,1,0,'N')
  ax.text(0,-1,0,'S')
  ax.set_xlim(-1,1)
  ax.set_ylim(-1,1)
  ax.set_zlim(0,1.5)
  plt.legend()
  plt.tight_layout()
  #plt.draw()
  plt.show(block=False)
  

def alt_az_to_lmn(alt,az,rho=1):
  """
  Convert alt az coordinates to lmn vector
  arg0: Altitude
  arg1: Azimut
  arg2(optional): rho, size of the vector (default:1)
  """
  az = az * math.pi/180
  alt = alt * math.pi/180
  y = rho * math.cos(alt) * math.cos(az)
  x = rho * math.cos(alt) * math.sin(az)
  z = rho * math.sin(alt) 

  lmn = (x,y,z)
  return lmn

def lmn_to_alt_az(lmn):
  """
  Convert lmn vector to alt az coordinates
  arg0: lmn vector
  """
  x,y,z = lmn
  az = math.atan2(x,y)
  if az <0:
    az = az + 2*math.pi
  alt = math.atan2(z,math.sqrt(x**2 + y**2))
  alt = alt * 180/math.pi 
  az =  az * 180/math.pi 
  return alt,az

def x_y_to_alt_az(x,y,mode,mesh_size):
  """
  Convert x y coordinates to alt az coordinates
  arg0: x
  arg1: y
  """
  if mode == nf.Mode.NEAR:
    center_x = 171
    center_y = 186
    az = math.atan2(center_x-x,center_y-y) * 180/math.pi +180
    return 0 , az

  elif mode == nf.Mode.FAR:
    center_x = mesh_size[0]//2
    center_y = mesh_size[1]//2
    az = math.atan2(x-center_x,y-center_y) * 180/ math.pi + 180
    alt = np.sqrt( (center_x-x)**2 + (center_y-y)**2 ) / (mesh_size[0]//180)
    alt = 90 - alt
    return alt,az








