import numpy as np
import scipy.signal

# CONSTANTS
Te = 5e-9
Fe = 1/Te
nof_feeds_per_files = 4
# compute number of spectrae and samples
polX="NE" # TODO Verifier les polars
polY="NO" # TODO Verifier les polars



def abs2(x):
  return x.real**2 + x.imag**2




def plot_temp_axe(files,n0,chunk_len,ax0,ax1,delais=None):
  nof_files = len(files)
  nof_feeds = nof_files * nof_feeds_per_files
  ax0_n = ax0.twiny()
  ax1_n = ax1.twiny()
  ratio = 1
  offset = 200 
  point_to_plot = chunk_len
  n0 += chunk_len//2 - point_to_plot//2
  for idx, file in enumerate(files):
    offsets = np.arange(idx*nof_feeds_per_files, (idx+1) * nof_feeds_per_files) * offset
    baselines = file[n0:n0+point_to_plot,:].mean(axis=0)
    n = np.arange(n0,n0+point_to_plot)
    ax0.plot(n - n[0], file[n0:n0+point_to_plot, 0::2] * ratio - baselines[0::2] + offsets, linewidth=0.2, c="k")
    ax1.plot(n - n[0], file[n0:n0+point_to_plot, 1::2] * ratio - baselines[1::2] + offsets, linewidth=0.2, c="k")
    if delais != None:
      for off_idx,off in enumerate(offsets):
        try:
          ax0.vlines(x = 1000 + 256 - delais[idx*len(offsets)+off_idx], ymin = off, ymax = off+200,colors = 'red')
        except:
          pass
  ax0.set_xlim((0, point_to_plot-1))
  ax0.set_ylim((-200,(nof_feeds)*offset))
  # Affichage des n° de MR sur l'axe verticale
  # C'est pas incroyable, mais sans se lancer dans des calculs savants c'est plus simple 
  MR_tick_pos = (-200, 1800, 3800,5800,7800,9800,11800,13800,15800,16000,16800)
  MR_tick = (-1,9,19,29,39,49,59,69,79,101,102)
  ax0.set_yticks(MR_tick_pos)
  ax0.set_yticklabels(MR_tick)

  ax0.set_title("Polar X")
  ax0.set_ylabel('N° MR')
  ax0.set_xlabel(f'Temps (#ech) + {n[0]}')

  ax1.set_xlim((0, point_to_plot-1))
  ax1.set_ylim((-200,(nof_feeds)*offset))
  ax1.set_yticks(MR_tick_pos)
  ax1.set_yticklabels(MR_tick)
  ax1.set_title("Polar Y")
  ax1.set_ylabel('N° MR')
  ax1.set_xlabel(f'Temps (#ech) + {n[0]}')




  for ax, ax_n in ((ax0, ax0_n), (ax1, ax1_n)):
    ax_n.set_xlim(ax.get_xlim())
    ax_n.set_xticks( ax.get_xticks() )
    ax_n.set_xbound(ax.get_xbound())
    n = ax.get_xticks()
    n -= n[0]
    ax_n.set_xticklabels(np.round(n * Te * 1e6,2))
    ax_n.set_xlabel(f'Temps (us)')
  return ax0,ax1

def plot_fft_axe(files,n0,chunk_len,ax0,ax1=None):
  nof_files = len(files)
  Nof_acc = 5
  N_fft = chunk_len // Nof_acc
  nof_spec = chunk_len // (Nof_acc * N_fft)
  f = np.arange(N_fft // 2 + 1) / N_fft * Fe
  W = scipy.signal.windows.blackmanharris(N_fft,sym=False)
  for idx, file in enumerate(files):
    for mr in range(4):
      dat = file[n0:n0+chunk_len, mr*2:mr*2+2]
      dat.shape = (nof_spec, Nof_acc, N_fft, 2)
      dat = dat * W[np.newaxis, :, np.newaxis]
      DAT = np.fft.rfft(dat,axis=2)
      DAT = abs2(DAT)
      DAT_acc = DAT.mean(axis=1)
      DAT_med = np.median(DAT.reshape((nof_spec*Nof_acc, N_fft // 2 + 1, 2)), axis=0)
  
      #fig, (ax0,ax1) = plt.subplots(nrows=2, figsize=(15,7))
      #ax0.imshow(10*np.log10(DAT_acc)[:,:,0].transpose(), cmap=plt.get_cmap('Greys_r'), aspect="equal", interpolation="nearest", origin='lower', vmin=20, vmax=90, extent=(0, chunk_len*Te*1e-3, 0, Fe/2))
      #ax1.imshow(10*np.log10(DAT_acc)[:,:,1].transpose(), cmap=plt.get_cmap('Greys_r'), aspect="equal", interpolation="nearest", origin='lower', vmin=20, vmax=90, extent=(0, chunk_len*Te*1e-3, 0, Fe/2))
      #ax0.set_aspect('auto')
      #ax0.set_title("Polar X")
      #ax0.set_ylabel('Frequency(MHz)')
  #
      #ax1.set_aspect('auto')
      #ax1.set_title("Polar Y")
      #ax1.set_ylabel('Frequency(MHz)')
      #plt.tight_layout()
      #plt.show()
      #fig, (ax0,ax1) = plt.subplots(nrows=2, figsize=(15,7))
      ax0.plot(f/1e6, 10*np.log10(DAT_acc)[:,:,0].transpose(), linewidth=0.2, c="k")
      #ax0.plot(f/1e6, 10*np.log10(DAT_med)[:,0], lw=2, c="w")
      ax0.set_ylim((20,90))
      ax0.set_title("Polar X")
      ax0.set_ylabel('Power (dB)')
      ax0.set_xlabel(f'f (MHz)')
      if ax1 != None:
        ax1.plot(f/1e6, 10*np.log10(DAT_acc)[:,:,1].transpose(), linewidth=0.2, c="k")
        #ax1.plot(f/1e6, 10*np.log10(DAT_med)[:,1], lw=2, c="w")
        ax1.set_ylim((20,90))
        ax1.set_title("Polar Y")
        ax1.set_ylabel('Power (dB)')
        ax1.set_xlabel(f'f (MHz)')
        #plt.tight_layout()
        #plt.show()

class fft:
  def __init__(self) -> None:
    pass

  def calcul(self,data):
    n_point = data.shape[0] 
    #data = data * W[np.newaxis, :, np.newaxis]
    self.f = np.arange(n_point // 2 + 1) / n_point * Fe
    W = scipy.signal.windows.blackmanharris(n_point,sym=False)
    if len(data.shape) == 3: # data not sommed
      data = data * W[:,np.newaxis,np.newaxis]
      fftx = np.fft.rfft(data[:,:,0],axis=0)
      ffty = np.fft.rfft(data[:,:,1],axis=0)
      fftx = abs2(fftx)
      ffty = abs2(ffty)
    elif len(data.shape) == 2: # data  sommed
      data = data * W[:,np.newaxis]
      fftx = np.fft.rfft(data[:,0],axis=0)
      ffty = np.fft.rfft(data[:,1],axis=0)
      fftx = abs2(fftx)
      ffty = abs2(ffty)
    
    return fftx,ffty

  def plot(self,fft,ax,color):
    ax.plot(self.f/1e6, 10*np.log10(fft),linewidth=0.5,color=color)


def plot_temp_axe_data(data,mr_list,offset,ratio,ax):
  nof_MR = data.shape[1]
  offsets = np.arange(0,offset*nof_MR,offset)
  abscisse = np.arange(0,(data.shape[0]*5e-9),5e-9)
  for idx in range(nof_MR):
    ax.plot(data[:,idx] * ratio + offsets[idx], linewidth=0.2, c="k")
    ax.set_ylim((-offset,(nof_MR)*offset))
    MR_tick_pos = offsets
    MR_tick = mr_list
    if len(mr_list) > 10:
      MR_tick_pos = offsets[::10]
      MR_tick = mr_list[::10]
    ax.set_yticks(MR_tick_pos)
    ax.set_yticklabels(MR_tick)
    ax.set_xlabel(f'Temps (ech 5ns')
    ax.set_ylabel(f'N° MR')


