from influxdb import InfluxDBClient,DataFrameClient
from datetime import datetime
import pandas as pd

class Database():

  def __init__(self,ip,port,user,psswd,db):
    self.db = db
    self.client = InfluxDBClient(ip, port, user, psswd, db)
    self.cliendf = DataFrameClient(ip,port,user,psswd,db)
    self.json_body = []

  def insert_event(self,time,measurement,tags,fields):
    data =  {
        "measurement": measurement,
        "tags": tags,
        "time": time ,
        "fields": fields
        }
    self.json_body.append(data)

  def push_events(self):
    result = self.client.write_points(self.json_body,time_precision='n')
    self.json_body = []
    assert result, "Error when push data to the DB"

  def query_between_start_end(self,measurement,start,stop=datetime.now(),fields="*",filters=''):
    query = f"SELECT {str(fields)} FROM {measurement} WHERE time >= '{start}' AND time <= '{stop}' {filters}"
    
    #print(query)
    result =  self.cliendf.query(query)
    if measurement in result:
      return result[measurement]
    else:
      result['no_events'] = True
      return result
  
