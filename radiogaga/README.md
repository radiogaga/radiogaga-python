# Modules radiogaga, classe et fonction diverses

## rolls

Module en pyc développé par @cviou. Permet de faire des numpy.roll mais sur un np.array à multi dimension. Cela a permis d'augmenter drastiquement la vitesse des farfield/nearfields

## altaz
Classe pour gérer les AltazA/AltazB

## database
Classe gérant la connection avec la base de donnée permettant de mettre en base chaque événement

## frame_parser
Classe pour parser toute les infos se trouvant dans le header des événements. Gère les Différentes versions de trames

## nearfield (à renommer)
Classe gérant les nearfield et les farfield

## pixel
Classe pour créér les vue dites "pixel" des DUMP TBB

## pointing
Différentes fonctions permettant de gérer toutes la parties MR, phasage, conversion LMN -> alt az etc

## rfi_multi (à renommer)
Fonctions permettant de plotter chaque forme d'onde de MR sur un seul plot

## spectrae
Fonctions pour faire des spectres et des TF avec les données TBB

## TBBurst_lib (à renommer)
Fonctions pour gérer toute la lecture des zips, .dat, parser Différentes infos (beaucoup de fonction plus utilisé)

## trigger
Classe reproduisant tous le processing fait par les cartes FPGA, pleins de signaux sont disponibles (phasés,sommés,filtré etc.). Fait aussi les calcul type SNr etc.