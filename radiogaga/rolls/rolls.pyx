# https://cython.readthedocs.io/en/latest/src/userguide/numpy_tutorial.html

# Cythonize with:
# > cython -a rolls.pyx
# inspect with:
# > firefox rolls.html
# generate lib with:
# > gcc -shared -pthread -fPIC -fwrapv -O2 -Wall -fno-strict-aliasing -I/usr/include/python3.8 -o rolls.so rolls.c


import numpy as np
cimport cython

ctypedef fused x_type:
    int
    float
    double
    long long
    float complex
    double complex

ctypedef fused shift_type:
    int
    long long


@cython.boundscheck(False)  # Deactivate bounds checking
@cython.wraparound(False)   # Deactivate negative indexing.
def rolls_cy(x_type[:, :] x, shift_type[:] shifts):
    """
    Roll every lines of array x by the values stored in shifts
    """
    cdef int nof_MR = x.shape[0]
    cdef shift_type vec_len = x.shape[1]

    assert nof_MR == shifts.shape[0]
    
    cdef shift_type shift
    cdef shift_type end

    if x_type is int:
        x_dtype = np.intc
    elif x_type is float:
        x_dtype = np.float32
    elif x_type is double:
        x_dtype = np.double
    elif x_type is cython.longlong:
        x_dtype = np.longlong
    elif x_type is cython.floatcomplex:
        x_dtype = np.complex64
    elif x_type is cython.doublecomplex:
        x_dtype = np.complex128


    y = np.empty((nof_MR, vec_len), dtype=x_dtype)
    cdef x_type[:, :] y_view = y

    for mr in range(nof_MR):
        #y[mr, :] = np.roll(x[mr,:], shifts[mr])
        shift = shifts[mr] % vec_len
        if shift == 0:
            y_view[mr,:] = x[mr,:]
        elif shift > 0:
            end = vec_len-shift
            y_view[mr, :shift] = x[mr,end:]
            y_view[mr, shift:] = x[mr,:end]
        else: # shift < 0:
            shift = -shift
            end = vec_len-shift
            y_view[mr, :end] = x[mr,shift:]
            y_view[mr, end:] = x[mr,:shift]
    return y
