# optimize multiple rolls on a 2D array 

import numpy as np
import importlib
import timeit

rolls = importlib.import_module('rolls')


def rolls_py(x, shifts):
    nof_MR, vec_len = x.shape
    y = np.empty_like(x)
    for mr in range(nof_MR):
        y[mr, :] = np.roll(x[mr,:], shifts[mr])
    return y


if __name__ == "__main__":

    data = np.array([
        [0, 0, 0, 1, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [1, 0, 0, 0, 0, 0],
        [1, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 0],
    ])
    delays = np.array([-2, 0, 1, 1, -1])

    my_rolls_py = rolls_py(data, delays)
    my_rolls_cy = rolls.rolls_cy(data, delays)
    assert (my_rolls_py == my_rolls_cy).all()


    nof_chans = 100
    nof_samples = 3000

    for data_dtype in ('float64', 'float32', 'int64', 'int32', 'int', 'complex64', 'complex128', ):
        for delay_dtype in ('int64', 'int32', 'int'):
            data = (1000*np.random.randn(nof_chans, nof_samples)).astype(data_dtype)
            delays = np.arange(-nof_chans/2, nof_chans/2, dtype=delay_dtype)

            my_rolls_py = rolls_py(data, delays)
            my_rolls_cy = rolls.rolls_cy(data, delays)
            assert (my_rolls_py == my_rolls_cy).all()

            t_base = timeit.timeit("rolls_py(data, delays)", globals=globals(), number=100)
            t_opt = timeit.timeit("rolls.rolls_cy(data, delays)", globals=globals(), number=100)
            print(f"data_dtype: {data_dtype : <10}, delay_dtype: {delay_dtype : <10}: Speedup = {t_base/t_opt : 4.2f}")

# %timeit rolls_py(data, delays)
# 1.02 ms ± 16.5 µs per loop (mean ± std. dev. of 7 runs, 1000 loops each)
# %timeit rolls_cy(data, delays)
# 42 µs ± 321 ns per loop (mean ± std. dev. of 7 runs, 10000 loops each)
